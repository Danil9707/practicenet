﻿using System;
using System.Collections.Generic;
using System.IO;

namespace MultiThreadingTask
{
    public class GenerateException
    {
        #region Fields

        private readonly Dictionary<System.Int32, Exception> _generate;
        private readonly Random _rndIndex;

        #endregion

        #region Constructor

        public GenerateException()
        {
            this._generate = new Dictionary<System.Int32, Exception>();
            _rndIndex = new Random();
            CreateDictionary();
        }

        #endregion

        #region Methods

        private void CreateDictionary()
        {
            _generate.Add(1, new FileNotFoundException());
            _generate.Add(2, new OutOfMemoryException());
            _generate.Add(3, new IndexOutOfRangeException());
            _generate.Add(4, new NullReferenceException());
            _generate.Add(5, new ArgumentOutOfRangeException());
            _generate.Add(6, new FileLoadException());
            _generate.Add(7, new FieldAccessException());
            _generate.Add(8, new DivideByZeroException());
            _generate.Add(9, new EntryPointNotFoundException());
            _generate.Add(10, new InvalidCastException());
            _generate.Add(11, new InsufficientMemoryException());
            _generate.Add(12, new MulticastNotSupportedException());
            _generate.Add(13, new KeyNotFoundException());
            _generate.Add(14, new DllNotFoundException());
            _generate.Add(15, new ArgumentNullException());
        }

        private Exception GetException()
        {
            return _generate[_rndIndex.Next(1, _generate.Count)];
        }

        public void ThrowException()
        {
            throw GetException();
        }

        #endregion



    }
}
