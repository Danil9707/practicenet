﻿namespace MultiThreadingTask
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.T_ErrorCollection = new System.Windows.Forms.TextBox();
            this.B_Start = new System.Windows.Forms.Button();
            this.B_Stop = new System.Windows.Forms.Button();
            this.Timer = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // T_ErrorCollection
            // 
            this.T_ErrorCollection.Location = new System.Drawing.Point(12, 12);
            this.T_ErrorCollection.Multiline = true;
            this.T_ErrorCollection.Name = "T_ErrorCollection";
            this.T_ErrorCollection.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.T_ErrorCollection.Size = new System.Drawing.Size(740, 245);
            this.T_ErrorCollection.TabIndex = 0;
            // 
            // B_Start
            // 
            this.B_Start.Location = new System.Drawing.Point(179, 276);
            this.B_Start.Name = "B_Start";
            this.B_Start.Size = new System.Drawing.Size(127, 34);
            this.B_Start.TabIndex = 1;
            this.B_Start.Text = "Start";
            this.B_Start.UseVisualStyleBackColor = true;
            this.B_Start.Click += new System.EventHandler(this.B_Start_Click);
            // 
            // B_Stop
            // 
            this.B_Stop.Location = new System.Drawing.Point(466, 276);
            this.B_Stop.Name = "B_Stop";
            this.B_Stop.Size = new System.Drawing.Size(127, 34);
            this.B_Stop.TabIndex = 2;
            this.B_Stop.Text = "Stop";
            this.B_Stop.UseVisualStyleBackColor = true;
            this.B_Stop.Click += new System.EventHandler(this.button2_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(764, 322);
            this.Controls.Add(this.B_Stop);
            this.Controls.Add(this.B_Start);
            this.Controls.Add(this.T_ErrorCollection);
            this.Name = "MainForm";
            this.Text = "Работа с потоками";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox T_ErrorCollection;
        private System.Windows.Forms.Button B_Start;
        private System.Windows.Forms.Button B_Stop;
        public System.Windows.Forms.Timer Timer;
    }
}

