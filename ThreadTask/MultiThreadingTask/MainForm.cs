﻿using System;
using System.Collections;
using System.Threading;
using System.Windows.Forms;

namespace MultiThreadingTask
{
    public partial class MainForm : Form
    {
        #region Field and Properties

        private readonly Thread _firstThread;
        private readonly Thread _secondThread;
        private static System.Boolean _isTurnOn;
        private readonly Mutex _mute;
        private volatile Queue _queue;
        private readonly GenerateException _exp;

        private String BlockingBox
        {
            set { T_ErrorCollection.Text += value; }

        }

        #endregion


        #region Delegate and Event

        private delegate void SetTextCallback();

        public event Action NotifyThread;

        #endregion


        #region Constructor

        public MainForm()
        {
            InitializeComponent();
            NotifyThread += SetTextInMainThread;

            _mute = new Mutex();
            _queue = new Queue();
            _exp = new GenerateException();
            _isTurnOn = false;

            _firstThread = new Thread(GenerateExceptions) {IsBackground = true, Name = "First Thread"};
            _secondThread = new Thread(GenerateExceptions) {IsBackground = true, Name = "Second Thread"};

            _firstThread.Start();
            _secondThread.Start();

        }

        #endregion
        

        #region Form event handlers

        private void button2_Click(object sender, EventArgs e)
        {
            _isTurnOn = false;
        }

        private void B_Start_Click(object sender, EventArgs e)
        {
            _isTurnOn = true;
        }

        #endregion

        
        #region Form Help methods

        private void SetTextInMainThread()
        {
            if (this.T_ErrorCollection.InvokeRequired)
            {
                SetTextCallback callback = SetTextInMainThread;
                this.Invoke(callback);
            }
            else
            {
                if (_queue.Count > 0)
                    this.BlockingBox = _queue.Dequeue().ToString();
            }


        }

        private void GenerateExceptions()
        {
            while (true)
            {
                if (!_isTurnOn)
                    continue;
                Thread.Sleep(2000);
                try
                {
                    _exp.ThrowException();

                }
                catch (Exception exp)
                {
                    _mute.WaitOne();
                    _queue.Enqueue($"Exception: {exp.Message} " +
                                   $"in Thread : Name {Thread.CurrentThread.Name}"
                                   + Environment.NewLine);
                    NotifyThread?.Invoke();
                    _mute.ReleaseMutex();
                }
            }
        }

        #endregion

        
    }
}
