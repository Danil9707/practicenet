﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace SerializationTask
{
    public interface IXmlDeserializationCallback
    {
        void OnXmlSerializa(Employee sender);
    }

    public class XmlHelper : XmlSerializer
    {
        #region Constructor

        public XmlHelper(Type type) : base(type)
        {
        }

        public XmlHelper(Type type, XmlRootAttribute root) : base(type, root)
        {
        }

        #endregion


        #region Method for Employees

        public void Serializas(String path, List<Employee> list)
        {

            foreach (var item in list)
            {
                var serializeCallback = item as IXmlDeserializationCallback;
                serializeCallback?.OnXmlSerializa(item);
            }
            using (var stream = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.Read))
            {
                base.Serialize(stream, list);
            }

        }

        #endregion

    }
}
