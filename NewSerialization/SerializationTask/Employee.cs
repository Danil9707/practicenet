﻿using System;

namespace SerializationTask
{
    
    [Serializable]
    public class Employee: IXmlDeserializationCallback
    {

        #region Properties and Fields

        private String _employeeId;
        public String EmployeeId { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Department { get; set; }
        public Int32 Age { get; set; }
        public String Address { get; set; }

        #endregion

        public override string ToString()
        {
            return $" Employee { FirstName + " "+ LastName}  Age: {Age} Work in Department {Department} Live at {Address}";
        }
        

        #region Implemention IXmlDeserializationCallback

        public void OnXmlSerializa(Employee sender)
        {
            
            if (sender == null)
            {
                return;
            }
            sender.EmployeeId = sender.LastName + " " + sender.FirstName;
        }

        #endregion



    }
}
