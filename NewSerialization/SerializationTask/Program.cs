﻿using System;

namespace SerializationTask
{
    class Program
    {
        static void Main(string[] args)
        {

            WorkWithSerialization list = new WorkWithSerialization();
            Console.WriteLine("Добавляем обекты ");

            list.AddEmployee(new Employee()
            {
                FirstName = "Danil",
                LastName = "Karachentsev",
                Address = "Kharkov",
                Department = ".Net",
                Age = 33
            });

            list.AddEmployee(new Employee()
            {
                FirstName = "Diana",
                LastName = "Karachentseva",
                Address = "Kharkov",
                Department = ".Net",
                Age = 34
            });
            Console.WriteLine(new String('-', 80));
            Console.WriteLine("Сериализуем объект");
            list.Serialization(list.GetBasePath);

            Console.WriteLine(new String('-', 80));
            Console.WriteLine("Десериализуем объект и показываем содержимое Xml файла");
            list.ShowCollection(list.GetBasePath);

            Console.WriteLine(new String('-', 80));
            list.SelectEmployees();
            Console.WriteLine("Выбираем сотрудников от 25 до 35 лет");
            list.Serialization(list.GetCustomPath);
            Console.WriteLine("Сериализуем объект");

            Console.WriteLine("Десериализуем объект и показываем содержимое Xml файла");
            list.ShowCollection(list.GetCustomPath);

            Console.WriteLine(new String('-', 80));
            Console.ReadLine();



        }
    }
}