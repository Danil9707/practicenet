﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Serialization;

namespace SerializationTask
{
    public class WorkWithSerialization
    {
        #region Fields and Properties

        private List<Employee> _employees;
        private XmlHelper _serializer;
        private readonly NameValueCollection _allAppSettings = ConfigurationManager.AppSettings;

        public String GetBasePath
        {
            get
            {
                return _allAppSettings["SerializationBase"];
                
            }
        }

        public String GetCustomPath
        {
            get
            {
                return _allAppSettings["CustomSerialization"];
            }
        }

        #endregion

        #region Help Methods

        private void Initialization()
        {
            _serializer =
                new XmlHelper(typeof(List<Employee>), new XmlRootAttribute("Employees"));
            _employees = new List<Employee>();
        }


        #endregion

        #region Constructor

        public WorkWithSerialization()
        {
            Initialization();
            if (File.Exists(GetBasePath))
            { 
                DeSerialization(GetBasePath);
            }
        }

        

        #endregion

        #region Work with List

        public void AddEmployee(Employee emp)
        {
            _employees.Add(emp);
        }

        public void SetEmployeesId()
        {
            foreach (var item in _employees)
            {
                SetEmployeeId(item);
            }
        }
        private String GetEmployeeId(Employee emp)
        {
            Type type = emp.GetType();
            FieldInfo employeeId = type.GetField("_employeeId", BindingFlags.Instance | BindingFlags.NonPublic);
            if (employeeId != null)
                return employeeId.GetValue(emp) as String;
            return String.Empty;
        }
        private void SetEmployeeId(Employee emp)
        {
            Type type = emp.GetType();
            FieldInfo employeeId = type.GetField("_employeeId", BindingFlags.Instance | BindingFlags.NonPublic);
            if (employeeId != null)
                employeeId.SetValue(emp, emp.EmployeeId);
        }
        public List<Employee> SelectEmployees()
        {
            var temp = _employees.
                Where(x => x.Age >= 25 && x.Age <= 35).OrderBy(x => GetEmployeeId(x)).ToList();
            _employees = temp;
            return temp;
        }

        #endregion


        #region Serialization Methods

        public void Serialization(String path)
        {
            _serializer.Serializas(path, _employees);
        }

        private List<Employee> DeSerialization(String path)
        {
            using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                var temp = _serializer.Deserialize(stream) as List<Employee>;
                _employees = temp;
                return temp;
            }
        }

        public void ShowCollection(String path)
        {

            var temp = DeSerialization(path);
            SetEmployeesId();
            foreach (var item in temp)
            {
                Type type = item.GetType();
                PropertyInfo address = type.GetProperty("Address", BindingFlags.Instance | BindingFlags.Public);
                Console.WriteLine
                    ($"ID: {GetEmployeeId(item)}" +
                     $" First Name: {item.FirstName} Last Name: {item.LastName}" +
                     $" Age: {item.Age} Department: {item.Department}" +
                     $" Address: {address?.GetValue(item).ToString()}");
            }
        }

        #endregion


    }
}
