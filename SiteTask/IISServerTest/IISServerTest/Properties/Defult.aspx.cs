﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IISServerTest.Properties
{
    public partial class Defult : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            Page.Response.Write($"Page_PreInit {DateTime.Now} <br />");
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            Page.Response.Write($"Page_Init {DateTime.Now} <br />");
        }

        protected void Page_InitComplete(object sender, EventArgs e)
        {
            Page.Response.Write($"Page_InitComplete {DateTime.Now} <br />");
        }

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            Page.Response.Write($"Page_PreLoad {DateTime.Now} <br />");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Response.Write($"Page_Load {DateTime.Now} <br />");

        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {

            Page.Response.Write($"Page_LoadComplete {DateTime.Now} <br />");
            if (IsPostBack)
            {
                var key = Page.Request.Form.AllKeys;
                Page.Response.Write("<table>");
                foreach (var item in key)
                {
                    Page.Response.Write($"<tr><td> Key: {item}</td> <td>Value: {Page.Request.Form[item]} </td></tr>");
                }
                Page.Response.Write("<table>");
            }
            //Page.Request.Form содержит в себе предидущее состояние страницы после IsPostBack
            //Оно запоминает в себе состояние всех елементов страницы , это так называемый ViewState
            //Например , если свойство Button имело значение "Button for PostBack", то это состояние будет занесено в 
            //Коллекцию и при PostBack запросе будет присвоено елементу Button
            
           

        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            Page.Response.Write($"Page_PreRender {DateTime.Now} <br />");
        }

        protected void Page_PreRenderComplete(object sender, EventArgs e)
        {
            Page.Response.Write($"Page_PreRenderComplete {DateTime.Now} <br />");

        }

        protected void Page_SaveStateComplete(object sender, EventArgs e)
        {
            Page.Response.Write($"Page_SaveStateComplete {DateTime.Now} <br />");

        }

        // Рендеринг страницы. Все элементы управления превращаются в HTML, CSS и JavaScript, который будет отправлен клиенту.
        //После этого события добавление в HTML разметку невозможно, поэтому если в Page_Unload вызвать Page.Response.Write
        //Произойдёт Exception

        protected void Page_Unload(object sender, EventArgs e)
        {
            //Page.Response.Write("Page_Unload <br />");
            // В этом событие происходит освобождение ресурсов, поэтому нельзя вызвать метод Page.Response.Write 

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Page.Response.Write("Is post back <br />");
        }
    }
}