﻿using System;
using System.Windows.Forms;

namespace UI
{
    public partial class ManagerContacts : Form
    {
        public ManagerContacts()
        {
            InitializeComponent();
            new Presenter(this);
        }


        public event EventHandler FormLoad;
        private void ManagerContacts_Load(object sender, EventArgs e)
        {
            FormLoad.Invoke(sender, e);
        }

        #region Work with list

        public event EventHandler AddContact;

        private void B_Add_Click(object sender, EventArgs e)
        {
            AddContact(sender, e);
        }

        public event EventHandler RemoveContact;

        private void B_Remove_Click(object sender, EventArgs e)
        {
            RemoveContact.Invoke(sender, e);
        }

        public event EventHandler SaveChangeContact;

        private void B_Save_Click(object sender, EventArgs e)
        {
            SaveChangeContact.Invoke(sender, e);
        }

        #endregion


        #region Form event handler

        public event EventHandler UpdateLoadPhoto;

        private void B_UpdateLoadPhoto_Click(object sender, EventArgs e)
        {
            UpdateLoadPhoto.Invoke(sender, e);
        }

        public event EventHandler CheckChange;

        private void R_Search_CheckedChanged(object sender, EventArgs e)
        {
            CheckChange.Invoke(sender, e);
        }

        public event EventHandler Filter;

        private void T_SearchFName_TextChanged(object sender, EventArgs e)
        {
            Filter.Invoke(sender, e);
        }

        public event TreeNodeMouseClickEventHandler LoadContact;

        private void ContactsList_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            LoadContact.Invoke(sender, e);
        }

        public event KeyPressEventHandler KeyValidate;

        private void T_FName_KeyPress(object sender, KeyPressEventArgs e)
        {
            KeyValidate.Invoke(sender, e);
        }

        #endregion

    }
}
