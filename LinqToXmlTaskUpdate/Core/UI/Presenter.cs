﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Core;

namespace UI
{
    public class Presenter
    {
        #region Fields

        private readonly ManagerContacts _view;
        private readonly WorkWithList _logic;
        private String _photo;
        private ContactManager _currentSelectedContact;

        #endregion

        #region Constructor

        public Presenter(ManagerContacts view)
        {
            _view = view;
            _logic = new WorkWithList(
                Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "managerContact.xml"));

            _view.FormLoad += _view__formLoad;
            _view.AddContact += _view__AddContact;
            _view.RemoveContact += _view__RemoveContact;
            _view.SaveChangeContact += _view__SaveChangeContact;
            _view.UpdateLoadPhoto += _view__UpdateLoadPhoto;
            _view.CheckChange += _view__CheckChange;
            _view.Filter += _view_Filter;
            _view.LoadContact += _view_LoadContact;
            _view.KeyValidate += _view_KeyValidate;
        }



        #endregion

        #region ManagerContact Event Handlers
        private void _view_KeyValidate(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == '.'))
            {
                e.Handled = true;
            }
        }
        private void _view_LoadContact(object sender, TreeNodeMouseClickEventArgs e)
        {
            WorkWithNode(e);
        }
        private void _view_Filter(object sender, EventArgs e)
        {
            Filter();
        }
        private void _view__CheckChange(object sender, EventArgs e)
        {
            var temp = sender as RadioButton;
            SetFilter(temp);
        }
        private void _view__UpdateLoadPhoto(object sender, EventArgs e)
        {
            SelectPhoto();
        }
        private void _view__SaveChangeContact(object sender, EventArgs e)
        {
            UpdateContact();
        }
        private void _view__RemoveContact(object sender, EventArgs e)
        {
            RemoveContact();
        }
        private void _view__AddContact(object sender, EventArgs e)
        {
            AddContact();
        }
        private void _view__formLoad(object sender, EventArgs e)
        {
            LoadToTreeListContact(_logic.GetCollection);
        }

        #endregion
      
        #region Help Methdos

        #region Work with TreeView

        private void LoadToTreeListContact(List<ContactManager> list)
        {
            _view.ContactsList.Nodes.Clear();
            foreach (var item in list)
            {
                var node = new TreeNode($"{item.FirstName} {item.LastName}");
                node.Nodes.Add(item.ContactId.ToString());
                node.Nodes.Add(item.Group);
                node.Nodes.Add(item.HomeTelefone);
                node.Nodes.Add(item.MobileTelefone);
                _view.ContactsList.Nodes.Add(node);
            }
        }
        private void LoadToTreeListContactByGroup()
        {
            _view.ContactsList.Nodes.Clear();
            foreach (var group in _logic.GetCollection.GroupBy(x => x.Group))
            {
                var node = new TreeNode(group.Key);
                foreach (var item in group)
                {
                    var temp = new TreeNode($"{item.FirstName} {item.LastName}");
                    temp.Nodes.Add(item.ContactId.ToString());
                    temp.Nodes.Add(item.HomeTelefone);
                    temp.Nodes.Add(item.MobileTelefone);
                    node.Nodes.Add(temp);
                }
                _view.ContactsList.Nodes.Add(node);

            }
        }
        private void Filter()
        {
            LoadToTreeListContact(_logic.GetCollection.Where(x =>
                x.FirstName.Contains(_view.T_SearchFName.Text) && x.LastName.Contains(_view.T_SearchLName.Text))
                .ToList());
        }
        private void SelectTreeView()
        {
            if (_view.R_GroupBy.Checked)
            {
                LoadToTreeListContactByGroup();
            }
            else
            {
                LoadToTreeListContact(_logic.GetCollection);
            }
        }
        private void WorkWithNode(TreeNodeMouseClickEventArgs e)
        {
            
            if (e.Node.Parent == null && !_view.R_GroupBy.Checked)
            {
                var temp = _logic.GetCollection.
                    FirstOrDefault(x => x.ContactId == Convert.ToInt32(e.Node.Nodes[0].Text)); 
                SetContact(temp);
                _currentSelectedContact = temp;
            }
            else if (e.Node.Parent != null && e.Node.Parent.Parent == null && _view.R_GroupBy.Checked)
            {
                var temp = _logic.GetCollection.FirstOrDefault
                    (x => x.ContactId == Convert.ToInt32(e.Node.Nodes[0].Text));
                SetContact(temp);
                _currentSelectedContact = temp;
            }
        }

        #endregion

        #region Work with standart element

        #region Work with contact

        private void AddContact()
        {
            if (!CheckField())
            {
                Console.WriteLine(@"Input all fields");
                return;
            }

            _logic.Add(CreateContact());
            ClearField();
            Console.WriteLine(@"Contact added");
            LoadToTreeListContact(_logic.GetCollection);
            _view.R_GroupBy.Checked = false;
            _view.R_Search.Checked = false;
        }

        private void UpdateContact()
        {
            if (_currentSelectedContact == null || !CheckField())
            {
                return;
            }
            _logic.EditElement(_currentSelectedContact, CreateContact());
            ClearField();
            SelectTreeView();
            _view.R_GroupBy.Checked = false;
            _view.R_Search.Checked = false;

        }

        private void RemoveContact()
        {
            if (_currentSelectedContact == null)
            {
                return;
            }
            _logic.Remove(_currentSelectedContact);
            ClearField();
            SelectTreeView();
            _view.R_GroupBy.Checked = false;
            _view.R_Search.Checked = false;
        }

        private ContactManager CreateContact()
        {
            return new ContactManager
            {
                ContactId = GetIdForContact(),
                FirstName = _view.T_FName.Text,
                LastName = _view.T_LName.Text,
                Group = _view.T_Group.Text,
                HomeTelefone = _view.T_HTelefone.Text,
                MobileTelefone = _view.T_MTelefon.Text,
                PhotoPath = _photo
            };
        }

        public Int32 GetIdForContact()
        {
            if (_logic.GetCollection.Count > 0)
                return _logic.GetCollection[_logic.GetCollection.Count - 1].ContactId + 1;
            else
                return 1;
        }
        #endregion

        #region Work with Image

        private void SetPhoto()
        {
            if (!File.Exists(_photo))
            {
                return;
            }
            _view.P_Image.SizeMode = PictureBoxSizeMode.StretchImage;
            _view.P_Image.Invalidate();
            _view.P_Image.Image = Image.FromFile(_photo);
        }

        private void SelectPhoto()
        {
            OpenFileDialog path = new OpenFileDialog
            {
                Filter = @"Image Files(*.BMP; *.JPG; *.GIF)| *.BMP; *.JPG; *.GIF "
            };

            path.ShowDialog();
            _photo = path.FileName;

            if (File.Exists(_photo))
            {
                var directory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ContactPhoto");

                var di = new DirectoryInfo(directory);
                di.Create();

                var imageName = Path.GetFileName(_photo);
                var appPath = directory + @"/" + imageName;

                if (!File.Exists(appPath))
                    File.Copy(_photo, appPath);
                _photo = appPath;
                SetPhoto();
            }

        }

        #endregion

        #region Work with form fields and elements

        private void ClearField()
        {
            _view.T_FName.Text = String.Empty;
            _view.T_LName.Text = String.Empty;
            _view.T_Group.Text = String.Empty;
            _view.T_HTelefone.Text = String.Empty;
            _view.T_MTelefon.Text = String.Empty;
            _photo = String.Empty;
            _view.P_Image.Image = null;
            _currentSelectedContact = null;
        }

        private void SetFilter(RadioButton temp)
        {
            if (temp.Checked && temp.Text == _view.R_GroupBy.Text)
            {
                _view.T_SearchFName.Text = String.Empty;
                _view.T_SearchLName.Text = String.Empty;

                _view.T_SearchLName.Enabled = false;
                _view.T_SearchFName.Enabled = false;

                LoadToTreeListContactByGroup();
                ClearField();
            }
            else
            {
                _view.T_SearchLName.Enabled = true;
                _view.T_SearchFName.Enabled = true;
                Filter();
                ClearField();
            }
        }

        private void SetContact(ContactManager contact)
        {
            _view.T_FName.Text = contact.FirstName;
            _view.T_LName.Text = contact.LastName;
            _view.T_Group.Text = contact.Group;
            _view.T_HTelefone.Text = contact.HomeTelefone;
            _view.T_MTelefon.Text = contact.MobileTelefone;
            _photo = contact.PhotoPath;
            SetPhoto();
        }

        private bool CheckField()
        {
            if (_view.T_FName.Text == String.Empty)
                return false;
            if (_view.T_LName.Text == String.Empty)
                return false;
            if (_view.T_Group.Text == String.Empty)
                return false;
            if (_view.T_HTelefone.Text == String.Empty)
                return false;
            if (_view.T_MTelefon.Text == String.Empty)
                return false;
            if (!File.Exists(_photo))
                return false;
            return true;
        }

        #endregion

        #endregion


        #endregion
    }
}
