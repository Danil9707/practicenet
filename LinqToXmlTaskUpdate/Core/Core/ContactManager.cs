﻿using System;

namespace Core
{
    public class ContactManager
    {
        #region Properties

        public Int32 ContactId { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Group { get; set; }
        public String HomeTelefone { get; set; }
        public String MobileTelefone { get; set; }
        public String PhotoPath { get; set; }

        #endregion

    }
}
