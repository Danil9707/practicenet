﻿using System;
using System.Collections.Generic;

namespace Core
{
    public class WorkWithList
    {
        #region Properties

        private List<ContactManager> _contactList;
        private readonly LinqToXmlWorker _helper;

       

        public List<ContactManager> GetCollection
        {
            get
            {
                return _contactList;
            }
        }
        #endregion


        public WorkWithList(String path)
        {
            _helper = new LinqToXmlWorker(path);
            LoadCollection();
            if (_contactList == null)
                _contactList = new List<ContactManager>();
        }

        #region Work with list methods

        public void Add(ContactManager element)
        {
            _contactList.Add(element);
            UpdateCollection();
        }

        public void Remove(ContactManager element)
        {
            _contactList.Remove(element);
            UpdateCollection();
        }

        public void EditElement(ContactManager editingElement, ContactManager newElement)
        {
            newElement.ContactId = editingElement.ContactId;
            _contactList.Remove(editingElement);
            _contactList.Add(newElement);
            UpdateCollection();
        }

        private void LoadCollection()
        {
            _contactList = _helper.DeserializeWithLinq();
        }

        private void UpdateCollection()
        {
            _helper.SerializeToXml(_contactList);
        }

        #endregion


    }
}
