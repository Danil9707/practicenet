﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace ValidatorTask
{
    public static class CreationControl
    {
        #region Create Form Controls

        public static void InitializeLabelControl(string id, string text, string cssClass, HtmlForm form1)
        {
            Label labelControl = new Label
            {
                ID = id,
                Text = text,
                CssClass = cssClass
            };
            form1.Controls.Add(labelControl);
        }

        public static void InitializeTextBox(string id, string cssClass, TextBoxMode mode, HtmlForm form1)
        {
            TextBox textBoxControl = new TextBox
            {
                ID = id,
                CssClass = cssClass,
                TextMode = mode
            };
            form1.Controls.Add(textBoxControl);
        }

        public static void CreateButton(string id, string text, string cssClass, EventHandler function, HtmlForm form1)
        {
            Button btn = new Button()
            {
                ID = id,
                Text = text,
                CssClass = cssClass

            };
            btn.Click += function;
            form1.Controls.Add(btn);
        }

        public static void CreateDropDownList(string id, List<string> data, string cssClass, EventHandler function,
            HtmlForm form1)
        {
            DropDownList ddl = new DropDownList
            {
                ID = id,
                CssClass = cssClass,
                DataSource = data,
                AutoPostBack = true
            };
            ddl.DataBind();
            ddl.SelectedIndexChanged += function;
            ddl.TextChanged += function;
            form1.Controls.Add(ddl);
        }

        #endregion

        #region Create Validator

        public static void CreateRegularValidator(string id, string targetControl, HtmlForm form1)
        {
            RegularExpressionValidator reg = new RegularExpressionValidator
            {
                ID = id,
                ErrorMessage = "Введите Email адрес еще раз.",
                ValidationExpression = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*",
                Display = ValidatorDisplay.None,
                ControlToValidate = targetControl
            };
            form1.Controls.Add(reg);
        }

        public static void CreateRequiredFieldValidator(string id, string targetElement, string errorMessage,
            HtmlForm form1)
        {
            RequiredFieldValidator field = new RequiredFieldValidator
            {
                ID = id,
                ControlToValidate = targetElement,
                ErrorMessage = errorMessage,
                Display = ValidatorDisplay.None
            };
            form1.Controls.Add(field);
        }

        public static void CreateSummaryValidator(HtmlForm form1)
        {
            ValidationSummary summery = new ValidationSummary
            {
                ID = "SummaryInfo",
                DisplayMode = ValidationSummaryDisplayMode.BulletList,
                ForeColor = Color.Green
            };
            form1.Controls.Add(summery);
        }

        public static void CreateComparerValidator(string id, string errorMessage, string targetElement,
            string compareElement, HtmlForm form1)
        {
            var validator = new CompareValidator
            {
                ID = id,
                ErrorMessage = errorMessage,
                ControlToCompare = compareElement,
                ControlToValidate = targetElement,
                Operator = ValidationCompareOperator.NotEqual,
                Display = ValidatorDisplay.None
            };
            form1.Controls.Add(validator);
        }

        public static void CreateCustomValidator(string id, string targetElement, string errorMessage, HtmlForm form1)
        {
            var validator = new RangeValidator
            {
                ID = id,
                ControlToValidate = targetElement,
                Type = ValidationDataType.Date,
                MaximumValue = DateTime.Now.Date.ToShortDateString(),
                MinimumValue = new DateTime(1960, 1, 1).ToShortDateString(),
                ErrorMessage = errorMessage,
                Display = ValidatorDisplay.None
            };
            form1.Controls.Add(validator);
        }

        #endregion
    }
}