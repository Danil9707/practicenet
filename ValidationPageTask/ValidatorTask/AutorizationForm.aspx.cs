﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ValidatorTask
{
    public partial class AutorizationForm : System.Web.UI.Page
    {
        private CountryCityDictionary count = new CountryCityDictionary();

        #region Page Event

        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager.ScriptResourceMapping.AddDefinition("jquery", new ScriptResourceDefinition
            {
                Path = "~/scripts/jquery-1.7.2.min.js",

            });
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            InitializeControls();

            InitializeButtons();

            InitializeValidators();
        }

        #endregion


        #region Support methods

        private void InitializeControls()
        {
            CreationControl.InitializeLabelControl("L_Login", "Псевдоним", "Label", form1);
            CreationControl.InitializeTextBox("Login", "TextBox", TextBoxMode.SingleLine, form1);

            CreationControl.InitializeLabelControl("L_Name", "Имя", "Label", form1);
            CreationControl.InitializeTextBox("Name", "TextBox", TextBoxMode.SingleLine, form1);

            CreationControl.InitializeLabelControl("L_LastName", "Фамилия", "Label", form1);
            CreationControl.InitializeTextBox("LastName", "TextBox", TextBoxMode.SingleLine, form1);

            CreationControl.InitializeLabelControl("L_Date", "Дата рождения", "Label", form1);
            CreationControl.InitializeTextBox("DateBirthsday", "TextBox", TextBoxMode.Date, form1);

            CreationControl.InitializeLabelControl("L_Email", "E-Mail", "Label", form1);
            CreationControl.InitializeTextBox("EMail", "TextBox", TextBoxMode.Email, form1);

            CreationControl.InitializeLabelControl("L_Country", "Страна", "Label", form1);
            CreationControl.CreateDropDownList("D_Country", count.GetCounty(), "DropDownList", UpdateItem, form1);

            CreationControl.InitializeLabelControl("L_City", "Город", "Label", form1);
            CreationControl.CreateDropDownList("D_City", count.GetCity("Украина"), "DropDownList", null, form1);
        }

        private void InitializeButtons()
        {
            CreationControl.CreateButton("ButtonSubmit", "Submit", "Button", Btn_Click, form1);

            CreationControl.CreateButton("ButtonReset", "Reset", "Button", Btn_Reset, form1);
        }

        private void InitializeValidators()
        {
            CreationControl.CreateRequiredFieldValidator
                ("EmptyLogin", "Login", "Поле псевдонима не заполненно", form1);
            CreationControl.CreateRequiredFieldValidator
                ("EmptyName", "Name", "Поле имя не заполненно", form1);
            CreationControl.CreateRequiredFieldValidator
                ("EmptyLastName", "LastName", "Поле фамилия не заполненно", form1);
            CreationControl.CreateRequiredFieldValidator
                ("EmptyEmail", "EMail", "Поле E-Mail не заполненно", form1);

            CreationControl.CreateComparerValidator
                ("NameValidator", "Псевдоним и Имя совподают", "Name", "Login", form1);
            CreationControl.CreateComparerValidator("LastNameValidator", "Фамилия и Имя совподают",
                "LastName", "Name", form1);

            CreationControl.CreateRegularValidator("EmailValidator", "EMail", form1);
            CreationControl.CreateCustomValidator("CustomDateValidator", "DateBirthsday",
                "Дата должна быть в диапазоне от 1/1/1960 до сегодняшнего дня", form1);
            CreationControl.CreateSummaryValidator(form1);
        }

        #endregion

        
        #region Page Handler

        private void UpdateItem(object sender, EventArgs e)
        {
            var item = (DropDownList) form1.FindControl("D_City");
            item.DataSource = count.GetCity((sender as DropDownList).SelectedValue);
            item.DataBind();
        }

        private void Btn_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Page.Title = "Авторизация прошла успешно";
            }
        }

        private void Btn_Reset(object sender, EventArgs e)
        {
            foreach (Control item in form1.Controls)
            {
                if (item is TextBox)
                {
                    ((TextBox) item).Text = String.Empty;
                }
            }
        }

        #endregion

        
    }


}