﻿using System;

namespace MultiplyTable
{
    class Program
    {
        static void Main(string[] args)
        {
            
            
            Console.WriteLine("Хотите использовать стандартную таблицу умножения от 1 до 10 или ввести свои значения Y/N ?");


            var item = new Int32[10];
            var print = new ShowTable();

            if (Console.ReadLine().ToLower() == "y")
            {
                try
                {
                    for (var i = 0; i < item.Length; i++)
                    {
                        Console.Write("{0} элемент = ",i+1);
                        item[i] = Convert.ToInt32(Console.ReadLine());
                    }
                    print = new ShowTable(item);
                }
                catch (Exception exp)
                {
                    Console.WriteLine(exp.ToString());
                }
             
            }
            
            print.PrintTableInForeach();

            Console.WriteLine(new String('_', 80));

            print.PrintWithWhile();

            Console.WriteLine(new String('_', 80));

            print.PrintTableInFor();

            Console.WriteLine(new String('_', 80));

            print.PrintWithDoWhile();

            Console.ReadLine();
        }
    }
}
