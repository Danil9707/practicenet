﻿using System;

namespace MultiplyTable
{
    public class ShowTable
    {
        #region Fields and properties

        private Int32[] _arr;

        #endregion

        #region Constructors
        public ShowTable()
        {
            _arr = new Int32[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        }

        public ShowTable(Int32[] arr)
        {
            _arr = arr;
        }
        #endregion


        #region MyRegion
        private void HelpCycl(Int32 digit, Int32 rowValue)
        {
            for (int i = 0; i < _arr.Length; i++)
            {
                Console.ForegroundColor =
                  rowValue == i ? ConsoleColor.Red : ConsoleColor.White;
                Console.Write("{0} \t", digit * _arr[i]);
            }

            Console.ForegroundColor = ConsoleColor.White;
        }
        #endregion



        #region PrintMethod

        public void PrintTableInForeach()
        {
            Console.WriteLine("Print with cycl Foreach");
            var rowValue = 0;
            foreach (var item in _arr)
            {
                HelpCycl(item,rowValue);
                rowValue++;
                Console.WriteLine();
            }
        }
        public void PrintTableInFor()
        {
            Console.WriteLine("Print with cycl For");
            for (var i = 0; i < _arr.Length; i++)
            {
                HelpCycl(_arr[i], i);
                Console.WriteLine();
            }
        }
        public void PrintWithDoWhile()
        {
            Console.WriteLine("Print with cycl Do While");
            var counter = 0;
            do
            {
                HelpCycl(_arr[counter],counter);
                counter++;
                Console.WriteLine();
            } while (counter < _arr.Length);
           
        }
        public void PrintWithWhile()
        {
            Console.WriteLine("Print with cycl While");
            var counter = 0;
            while (counter < _arr.Length)
            {
                HelpCycl(_arr[counter],counter);
                counter++;
                Console.WriteLine();
            }
            
        }
        #endregion
    }
}
