﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace Core
{
    internal class LinqToXmlWorker
    {
        private readonly String _path;

        public LinqToXmlWorker(string path)
        {
            _path = path;
        }

        #region Linq to XML methods

        public void SerializeToXml(List<ContactManager> contactList)
        {
            var xmlDocument = new XDocument
                (
                new XDeclaration("1.0", "utf-8", "yes"),
                new XComment("Creating an XML Tree using Linq to Xml"),
                new XElement("ManagerContacts",
                    from contacts in contactList
                    select new XElement("Contact",
                        new XElement("FirstName", contacts.FirstName),
                        new XElement("LastName", contacts.LastName),
                        new XElement("Group", contacts.Group),
                        new XElement("HomeTelefone", contacts.HomeTelefone),
                        new XElement("MobileTelefone", contacts.MobileTelefone),
                        new XElement("PhotoPath", contacts.PhotoPath)
                        )
                    )
                );
            xmlDocument.Save(_path);

        }

        public List<ContactManager> DeserializeWithLinq()
        {
            if (!File.Exists(_path))
                return null;
            return XDocument.Load(_path).Descendants("Contact").Select(x => new ContactManager
            {
                FirstName = x.Element("FirstName").Value,
                LastName = x.Element("LastName").Value,
                Group = x.Element("Group").Value,
                HomeTelefone = x.Element("HomeTelefone").Value,
                MobileTelefone = x.Element("MobileTelefone").Value,
                PhotoPath = x.Element("PhotoPath").Value
            }).ToList();
        }


        #endregion


    }
}
