﻿using System;

namespace Core
{
    public class ContactManager
    {
        #region Properties

        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Group { get; set; }
        public String HomeTelefone { get; set; }
        public String MobileTelefone { get; set; }
        public String PhotoPath { get; set; }

        #endregion

    }
}
