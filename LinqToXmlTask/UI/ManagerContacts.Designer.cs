﻿namespace UI
{
    partial class ManagerContacts
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ManagerContacts));
            this.ContactsList = new System.Windows.Forms.TreeView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.T_SearchFName = new System.Windows.Forms.TextBox();
            this.T_SearchLName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.B_Save = new System.Windows.Forms.Button();
            this.B_Remove = new System.Windows.Forms.Button();
            this.B_Add = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.R_Search = new System.Windows.Forms.RadioButton();
            this.R_GroupBy = new System.Windows.Forms.RadioButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.T_MTelefon = new System.Windows.Forms.MaskedTextBox();
            this.T_HTelefone = new System.Windows.Forms.MaskedTextBox();
            this.B_UpdateLoadPhoto = new System.Windows.Forms.Button();
            this.P_Image = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.T_Group = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.T_FName = new System.Windows.Forms.TextBox();
            this.T_LName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.P_Image)).BeginInit();
            this.SuspendLayout();
            // 
            // ContactsList
            // 
            this.ContactsList.Location = new System.Drawing.Point(12, 8);
            this.ContactsList.Name = "ContactsList";
            this.ContactsList.Size = new System.Drawing.Size(239, 411);
            this.ContactsList.TabIndex = 0;
            this.ContactsList.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.ContactsList_NodeMouseClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.groupBox6);
            this.groupBox1.Controls.Add(this.groupBox5);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.R_Search);
            this.groupBox1.Controls.Add(this.R_GroupBy);
            this.groupBox1.Location = new System.Drawing.Point(261, 268);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(402, 151);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Фильтрация";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.T_SearchFName);
            this.groupBox2.Controls.Add(this.T_SearchLName);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(6, 36);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(158, 86);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            // 
            // T_SearchFName
            // 
            this.T_SearchFName.Location = new System.Drawing.Point(6, 18);
            this.T_SearchFName.Name = "T_SearchFName";
            this.T_SearchFName.Size = new System.Drawing.Size(140, 20);
            this.T_SearchFName.TabIndex = 4;
            this.T_SearchFName.TextChanged += new System.EventHandler(this.T_SearchFName_TextChanged);
            this.T_SearchFName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.T_FName_KeyPress);
            // 
            // T_SearchLName
            // 
            this.T_SearchLName.Location = new System.Drawing.Point(6, 60);
            this.T_SearchLName.Name = "T_SearchLName";
            this.T_SearchLName.Size = new System.Drawing.Size(140, 20);
            this.T_SearchLName.TabIndex = 3;
            this.T_SearchLName.TextChanged += new System.EventHandler(this.T_SearchFName_TextChanged);
            this.T_SearchLName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.T_FName_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(5, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Фамилия";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(3, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Имя";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.B_Save);
            this.groupBox6.Controls.Add(this.B_Remove);
            this.groupBox6.Controls.Add(this.B_Add);
            this.groupBox6.Location = new System.Drawing.Point(190, 14);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(201, 121);
            this.groupBox6.TabIndex = 2;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Основные функции";
            // 
            // B_Save
            // 
            this.B_Save.Location = new System.Drawing.Point(10, 69);
            this.B_Save.Name = "B_Save";
            this.B_Save.Size = new System.Drawing.Size(86, 39);
            this.B_Save.TabIndex = 8;
            this.B_Save.Text = "Сохранить изменения";
            this.B_Save.UseVisualStyleBackColor = true;
            this.B_Save.Click += new System.EventHandler(this.B_Save_Click);
            // 
            // B_Remove
            // 
            this.B_Remove.Location = new System.Drawing.Point(108, 32);
            this.B_Remove.Name = "B_Remove";
            this.B_Remove.Size = new System.Drawing.Size(86, 27);
            this.B_Remove.TabIndex = 9;
            this.B_Remove.Text = "Удалить";
            this.B_Remove.UseVisualStyleBackColor = true;
            this.B_Remove.Click += new System.EventHandler(this.B_Remove_Click);
            // 
            // B_Add
            // 
            this.B_Add.Location = new System.Drawing.Point(10, 32);
            this.B_Add.Name = "B_Add";
            this.B_Add.Size = new System.Drawing.Size(86, 27);
            this.B_Add.TabIndex = 7;
            this.B_Add.Text = "Добавить";
            this.B_Add.UseVisualStyleBackColor = true;
            this.B_Add.Click += new System.EventHandler(this.B_Add_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.button5);
            this.groupBox5.Controls.Add(this.button6);
            this.groupBox5.Controls.Add(this.button7);
            this.groupBox5.Location = new System.Drawing.Point(190, 15);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(201, 121);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Основные функции";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(10, 69);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(86, 27);
            this.button5.TabIndex = 2;
            this.button5.Text = "Сохранить";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(108, 32);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(86, 27);
            this.button6.TabIndex = 1;
            this.button6.Text = "Удалить";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(10, 32);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(86, 27);
            this.button7.TabIndex = 0;
            this.button7.Text = "Добавить";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button3);
            this.groupBox3.Controls.Add(this.button2);
            this.groupBox3.Controls.Add(this.button1);
            this.groupBox3.Location = new System.Drawing.Point(190, 15);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(201, 121);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Основные функции";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(10, 69);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(86, 27);
            this.button3.TabIndex = 2;
            this.button3.Text = "Сохранить";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(108, 32);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(86, 27);
            this.button2.TabIndex = 1;
            this.button2.Text = "Удалить";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(10, 32);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(86, 27);
            this.button1.TabIndex = 0;
            this.button1.Text = "Добавить";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // R_Search
            // 
            this.R_Search.AutoSize = true;
            this.R_Search.Location = new System.Drawing.Point(6, 19);
            this.R_Search.Name = "R_Search";
            this.R_Search.Size = new System.Drawing.Size(98, 17);
            this.R_Search.TabIndex = 0;
            this.R_Search.TabStop = true;
            this.R_Search.Text = "По инициалам";
            this.R_Search.UseVisualStyleBackColor = true;
            this.R_Search.CheckedChanged += new System.EventHandler(this.R_Search_CheckedChanged);
            // 
            // R_GroupBy
            // 
            this.R_GroupBy.AutoSize = true;
            this.R_GroupBy.Location = new System.Drawing.Point(5, 128);
            this.R_GroupBy.Name = "R_GroupBy";
            this.R_GroupBy.Size = new System.Drawing.Size(139, 17);
            this.R_GroupBy.TabIndex = 1;
            this.R_GroupBy.TabStop = true;
            this.R_GroupBy.Text = "Объеденить по группе";
            this.R_GroupBy.UseVisualStyleBackColor = true;
            this.R_GroupBy.CheckedChanged += new System.EventHandler(this.R_Search_CheckedChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.T_MTelefon);
            this.groupBox4.Controls.Add(this.T_HTelefone);
            this.groupBox4.Controls.Add(this.B_UpdateLoadPhoto);
            this.groupBox4.Controls.Add(this.P_Image);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.T_Group);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.T_FName);
            this.groupBox4.Controls.Add(this.T_LName);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Location = new System.Drawing.Point(261, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(401, 259);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Основная информация";
            // 
            // T_MTelefon
            // 
            this.T_MTelefon.Location = new System.Drawing.Point(15, 231);
            this.T_MTelefon.Mask = "+38(\\099) 000-0000";
            this.T_MTelefon.Name = "T_MTelefon";
            this.T_MTelefon.Size = new System.Drawing.Size(137, 20);
            this.T_MTelefon.TabIndex = 5;
            // 
            // T_HTelefone
            // 
            this.T_HTelefone.Location = new System.Drawing.Point(15, 184);
            this.T_HTelefone.Mask = "+(0) 00-00-00";
            this.T_HTelefone.Name = "T_HTelefone";
            this.T_HTelefone.Size = new System.Drawing.Size(137, 20);
            this.T_HTelefone.TabIndex = 4;
            // 
            // B_UpdateLoadPhoto
            // 
            this.B_UpdateLoadPhoto.Location = new System.Drawing.Point(244, 214);
            this.B_UpdateLoadPhoto.Name = "B_UpdateLoadPhoto";
            this.B_UpdateLoadPhoto.Size = new System.Drawing.Size(126, 35);
            this.B_UpdateLoadPhoto.TabIndex = 6;
            this.B_UpdateLoadPhoto.Text = "Добавить/Обновить фото";
            this.B_UpdateLoadPhoto.UseVisualStyleBackColor = true;
            this.B_UpdateLoadPhoto.Click += new System.EventHandler(this.B_UpdateLoadPhoto_Click);
            // 
            // P_Image
            // 
            this.P_Image.ErrorImage = ((System.Drawing.Image)(resources.GetObject("P_Image.ErrorImage")));
            this.P_Image.Location = new System.Drawing.Point(225, 16);
            this.P_Image.Name = "P_Image";
            this.P_Image.Size = new System.Drawing.Size(156, 188);
            this.P_Image.TabIndex = 15;
            this.P_Image.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(12, 162);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(127, 16);
            this.label7.TabIndex = 13;
            this.label7.Text = "Домашний телефон";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(12, 212);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(131, 16);
            this.label6.TabIndex = 11;
            this.label6.Text = "Мобильный телефон";
            // 
            // T_Group
            // 
            this.T_Group.Location = new System.Drawing.Point(12, 128);
            this.T_Group.Name = "T_Group";
            this.T_Group.Size = new System.Drawing.Size(140, 20);
            this.T_Group.TabIndex = 3;
            this.T_Group.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.T_FName_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(12, 112);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 16);
            this.label5.TabIndex = 9;
            this.label5.Text = "Группа";
            // 
            // T_FName
            // 
            this.T_FName.Location = new System.Drawing.Point(12, 32);
            this.T_FName.Name = "T_FName";
            this.T_FName.Size = new System.Drawing.Size(140, 20);
            this.T_FName.TabIndex = 1;
            this.T_FName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.T_FName_KeyPress);
            // 
            // T_LName
            // 
            this.T_LName.Location = new System.Drawing.Point(12, 78);
            this.T_LName.Name = "T_LName";
            this.T_LName.Size = new System.Drawing.Size(140, 20);
            this.T_LName.TabIndex = 2;
            this.T_LName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.T_FName_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(12, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "Фамилия";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(12, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 16);
            this.label4.TabIndex = 5;
            this.label4.Text = "Имя";
            // 
            // ManagerContacts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(675, 426);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ContactsList);
            this.Name = "ManagerContacts";
            this.Text = "Контакты менеджера";
            this.Load += new System.EventHandler(this.ManagerContacts_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.P_Image)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button B_UpdateLoadPhoto;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button B_Save;
        private System.Windows.Forms.Button B_Remove;
        private System.Windows.Forms.Button B_Add;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        public System.Windows.Forms.TreeView ContactsList;
        public System.Windows.Forms.TextBox T_SearchFName;
        public System.Windows.Forms.TextBox T_SearchLName;
        public System.Windows.Forms.PictureBox P_Image;
        public System.Windows.Forms.TextBox T_Group;
        public System.Windows.Forms.TextBox T_FName;
        public System.Windows.Forms.TextBox T_LName;
        public System.Windows.Forms.RadioButton R_Search;
        public System.Windows.Forms.RadioButton R_GroupBy;
        public System.Windows.Forms.MaskedTextBox T_MTelefon;
        public System.Windows.Forms.MaskedTextBox T_HTelefone;
    }
}

