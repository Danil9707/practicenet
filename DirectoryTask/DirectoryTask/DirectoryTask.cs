﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DirectoryTask
{
    public partial class DirectoryTask : Form
    {
        #region Fields and Properties

        private TreeNode tree = null;
        private static Boolean _isRecord;
        private static String _path;
        private static DirectoryInfo _info;
        private Thread serializeToXml = null;
        private Thread loadToTreeView = null;
        private Thread selectDirectory = null;

        #endregion


        #region Event and Handler

        public event Action<Exception> ExceptionIvent;

        private delegate void ExceptionDelegate(Exception exp);

        private delegate void SetTextCallback();

        public event Action NotifyThread;

        #endregion


        public DirectoryTask()
        {
            InitializeComponent();
            Initialization();
        }


        #region Form Handlers

        private void SelectDirectory(object sender, EventArgs e)
        {
            SelectDirectory();
        }

        private void PathToSave(object sender, EventArgs e)
        {
            SelectFilePath();
        }

        #endregion


        #region Help methods

        private void LoadToTreeView()
        {
            if (this.treeView1.InvokeRequired)
            {
                SetTextCallback callback = LoadToTreeView;
                this.Invoke(callback);
            }
            else
            {
                treeView1.Nodes.Add(tree);
            }


        }

        private async void WorkWithTreeView()
        {
            try
            {
                while (true)
                {
                    if (!_isRecord)
                        continue;
                    NotifyThread.Invoke();
                }
            }
            catch (Exception exp)
            {
                ExceptionIvent.Invoke(exp);
            }

        }

        private async void SerializeToXml()
        {
            try
            {
                while (true)
                {
                    if (!_isRecord)
                        continue;
                    Logic.TreeViewToXml(tree, _path);
                    Reset();
                }
            }
            catch (Exception exp)
            {
                ExceptionIvent.Invoke(exp);
            }

        }

        private void Reset()
        {
            _isRecord = false;
            _path = String.Empty;
            _info = null;
            tree = null;
        }

        private void LoadToLocalTree()
        {
            tree = Logic.GetFullDirectoryInfo(_info);

        }

        public void ClearTree()
        {
            treeView1.Nodes.Clear();
        }

        private void Initialization()
        {
            serializeToXml = new Thread(SerializeToXml)
            {
                Name = "XmlThread",
                IsBackground = true
            };

            loadToTreeView = new Thread(WorkWithTreeView)
            {
                Name = "TreeThread",
                IsBackground = true
            };
            _isRecord = false;
            serializeToXml.Start();
            loadToTreeView.Start();
            NotifyThread += LoadToTreeView;
            ExceptionIvent += Logic.ExceptionHandler;
        }

        private void SelectDirectory()
        {
            try
            {

                if (treeView1.Nodes.Count != 0)
                    ClearTree();
                var folder = new FolderBrowserDialog();
                folder.ShowDialog();
                if (String.IsNullOrEmpty(folder.SelectedPath))
                    return;
                _info = new DirectoryInfo(folder.SelectedPath);
          
                selectDirectory = new Thread(LoadToLocalTree) { IsBackground = true };
                selectDirectory.Start();
                while (tree == null)
                {

                }
                if (String.IsNullOrEmpty(_path))
                {
                    MessageBox.Show("Выберите путь для сохранения файла");
                    return;
                }
                _isRecord = true;
                
            }
            catch (Exception exp)
            {
                ExceptionIvent.Invoke(exp);
            }

        }

        private void SelectFilePath()
        {
            try
            {
                var file = new SaveFileDialog()
                {
                    Filter = @"XML files(*.XML)| *.XML "
                };
                file.ShowDialog();
                if (String.IsNullOrEmpty(file.FileName))
                    return;
                _path = file.FileName;

                if (tree == null)
                {
                    MessageBox.Show("Выберите директорию");
                    return;
                }
                _isRecord = true;

            }
            catch (Exception exp)
            {
                ExceptionIvent.Invoke(exp);
            }
        }


        #endregion


    }
}
