﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace DirectoryTask
{
    public static class Logic
    {
        private static XmlDocument _xmlDocument;
        private static Mutex mute;

        static Logic()
        {
            mute = new Mutex();
        }
        #region Extention methods
        
        public static void ExceptionHandler(this Exception exp)
        {
            mute.WaitOne();
            MessageBox.Show($"Exception: {exp.Message} InMethods: {exp.TargetSite}");
            mute.ReleaseMutex();
        }
        
        #endregion


        #region Create TreeNode collection

        public static TreeNode GetFullDirectoryInfo(DirectoryInfo info)
        {
            var treeRoot = new TreeNode(info.Name);
            if (info.GetFiles().Length != 0)
                GetFileInfo(info.GetFiles(), treeRoot);
            if (info.GetDirectories().Length == 0) return treeRoot;
            foreach (var item in info.GetDirectories())
            {
                treeRoot.Nodes.Add(GetFullDirectoryInfo(item));
            }
            return treeRoot;
        }

        private static void GetFileInfo(IEnumerable<FileInfo> fileInfo, TreeNode node)
        {
            var fileInThisDirectory = new TreeNode("FileInThisDirectory");
            foreach (var infoFile in fileInfo)
            {
                fileInThisDirectory.Nodes.Add($"File:{infoFile.Name}" +
                                              $" Date Create:{infoFile.CreationTime}" +
                                              $" Size: {infoFile.Length}");
            }
            node.Nodes.Add(fileInThisDirectory);
        }

        #endregion

        #region Convert TreeNode to Xml file methods

        public static void TreeViewToXml(TreeNode treeView, String path)
        {
            _xmlDocument = new XmlDocument();
            _xmlDocument.AppendChild(_xmlDocument.CreateElement("SelectedDirectory"));
            XmlRekursivExport(_xmlDocument.DocumentElement, treeView.Nodes);
            _xmlDocument.Save(path);
        }

        private static void XmlRekursivExport(XmlNode nodeElement, TreeNodeCollection treeNodeCollection)
        {
            foreach (TreeNode treeNode in treeNodeCollection)
            {
                XmlNode xmlNode = null;
                if (treeNode.Nodes.Count != 0)
                    xmlNode = _xmlDocument.CreateElement(treeNode.Text);
                else
                {
                    xmlNode = _xmlDocument.CreateElement("Item");
                    xmlNode.Attributes.Append(_xmlDocument.CreateAttribute("File"));
                    xmlNode.Attributes["File"].Value = treeNode.Text;
                }

                nodeElement?.AppendChild(xmlNode);

                if (treeNode.Nodes.Count > 0)
                {
                    XmlRekursivExport(xmlNode, treeNode.Nodes);
                }
            }
        }

        #endregion
    }
}
