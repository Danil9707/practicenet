﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace SerializationTask
{
    public interface IXmlDeserializationCallback
    {
        void OnXmlDeserialization(Employee sender);
    }

    public class XmlHelper : XmlSerializer
    {
        #region Constructor

        public XmlHelper(Type type) : base(type)
        {
        }

        public XmlHelper(Type type, XmlRootAttribute root) : base(type, root)
        {
        }

        #endregion


        #region Method for Employees

        public List<Employee> Deserializes(Stream reader)
        {
            var result = base.Deserialize(reader) as List<Employee>;
            if (result == null)
            {
                return null;
            }
            foreach (var item in result)
            {
                var deserializedCallback = item as IXmlDeserializationCallback;
                deserializedCallback?.OnXmlDeserialization(item);
            }
            return result;
        }

        #endregion

    }
}
