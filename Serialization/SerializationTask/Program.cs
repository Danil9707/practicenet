﻿using System;

namespace SerializationTask
{
    class Program
    {
        static void Main(string[] args)
        {

            WorkWithSerialization list = new WorkWithSerialization();
            Console.WriteLine("Добавляем обекты ");
            list.AddEmployee(new Employee()
            {
                FirstName = "Danil",
                LastName = "Zelentskiy",
                Age = 26,
                Department = ".Net Developer",
                Address = "Kharkov Studentskaya Work"
            });
            list.AddEmployee(new Employee()
            {
                FirstName = "Diana",
                LastName = "Zelentskaya",
                Age = 27,
                Department = ".Net Developer",
                Address = "Kharkov Studentskaya Work"
            });
            list.AddEmployee(new Employee()
            {
                FirstName = "Artem",
                LastName = "Marackhovskiy",
                Age = 45,
                Department = ".Net Developer",
                Address = "Kharkov Herous Work"
            });
            Console.WriteLine(new String('-', 80));
            Console.WriteLine("Сериализуем объект");
            list.Serialization(false);

            Console.WriteLine(new String('-', 80));
            Console.WriteLine("Десериализуем объект и показываем содержимое Xml файла");
            list.ShowCollection(false);

            Console.WriteLine(new String('-', 80));
            list.SelectEmployees();
            Console.WriteLine("Выбираем сотрудников от 25 до 35 лет");
            list.Serialization(true);
            Console.WriteLine("Сериализуем объект");

            Console.WriteLine("Десериализуем объект и показываем содержимое Xml файла");
            list.ShowCollection(true);

            Console.WriteLine(new String('-', 80));
            Console.ReadLine();



        }
    }
}