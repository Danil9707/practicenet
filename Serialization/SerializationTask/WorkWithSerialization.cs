﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Serialization;

namespace SerializationTask
{
    public class WorkWithSerialization
    {
        #region Fields and Properties

        private List<Employee> _employees;
        private XmlHelper _serializer;
        private String _customSerializationPath;
        private String _serializationPath;
        private readonly NameValueCollection _allAppSettings = ConfigurationManager.AppSettings;

        #endregion

        #region Help Methods

        private void Initialization()
        {
            _serializer =
                new XmlHelper(typeof(List<Employee>), new XmlRootAttribute("Employees"));
            _employees = new List<Employee>();
        }

        private void SetPath()
        {
            _customSerializationPath = _allAppSettings["CustomSerialization"];
            _serializationPath = _allAppSettings["SerializationBase"];
        }

        #endregion

        #region Constructor

        public WorkWithSerialization()
        {
            Initialization();
            SetPath();
            if (File.Exists(_serializationPath))
                DeSerialization(false);
        }

        public WorkWithSerialization(String pathFileSerialization, String pathFileCustomSerialization)
        {
            Initialization();
            _customSerializationPath = pathFileCustomSerialization;
            _serializationPath = pathFileSerialization;
            _allAppSettings["CustomSerialization"] = _customSerializationPath;
            _allAppSettings["SerializationBase"] = _serializationPath;
            if (File.Exists(_serializationPath))
                DeSerialization(false);
        }

        #endregion

        #region Work with List

        public void AddEmployee(Employee emp)
        {
            _employees.Add(emp);
        }

        private String GetEmployeeId(Employee emp)
        {
            Type type = emp.GetType();
            FieldInfo employeeId = type.GetField("_employeeId", BindingFlags.Instance | BindingFlags.NonPublic);
            if (employeeId != null)
                return employeeId.GetValue(emp) as String;
            return String.Empty;
        }

        public List<Employee> SelectEmployees()
        {
            var temp = _employees.
                Where(x => x.Age >= 25 && x.Age <= 35).OrderBy(x => GetEmployeeId(x)).ToList();
            _employees = temp;
            return temp;
        }

        #endregion


        #region Serialization Methods

        public void Serialization(Boolean isCustom)
        {
            var path = String.Empty;
            path = !isCustom ? _serializationPath : _customSerializationPath;
            using (var stream = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.Read))
            {
                _serializer.Serialize(stream, _employees);
            }
        }

        public List<Employee> DeSerialization(Boolean isCustom)
        {
            var path = String.Empty;
            path = !isCustom ? _serializationPath : _customSerializationPath;
            using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                var temp = _serializer.Deserializes(stream);
                _employees = temp;
                return temp;
            }
        }

        public void ShowCollection(Boolean isCustom)
        {

            var temp = DeSerialization(isCustom);
            foreach (var item in temp)
            {
                Type type = item.GetType();
                PropertyInfo address = type.GetProperty("Address", BindingFlags.Instance | BindingFlags.Public);
                Console.WriteLine
                    ($"ID: {GetEmployeeId(item)}" +
                     $" First Name: {item.FirstName} Last Name: {item.LastName}" +
                     $" Age: {item.Age} Department: {item.Department}" +
                     $" Address: {address?.GetValue(item).ToString()}");
            }
        }

        #endregion


    }
}
