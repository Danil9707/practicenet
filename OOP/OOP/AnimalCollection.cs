﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    public class AnimalsCollection
    {
        private List<AliveCreature> _animals;

        public AnimalsCollection()
        {
            _animals = new List<AliveCreature>();
        }

        public void AddAnimal(AliveCreature creature)
        {
            _animals.Add(creature);
        }

        public void ShowAnimalBreatheUnderwater()
        {
            foreach (var item in _animals)
            {
                if (item is IBreathe)
                {
                    Console.WriteLine("I can breathe undrerwated and I am {0}",item.ToString());
                }
            }
        }

        public void CalculateAnimalPaws()
        {
            var countPaws = _animals.OfType<Animal>().Sum(item => 4);
            Console.WriteLine("Count paws in my collection : {0}",countPaws);
        }
    }
}
