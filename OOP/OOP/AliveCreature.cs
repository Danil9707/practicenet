﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    public interface IBreathe
    {
        void BreatheUnderWater();
    }
    public interface IEat
    {
        void EatingHay();
    }
    public abstract class AliveCreature
    {
        public String CreatureName { get; set; }
    }
    
}
