﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    class Program
    {
        static void Main(string[] args)
        {
            AnimalsCollection animals = new AnimalsCollection();

            animals.AddAnimal(new Dog() {CreatureName = "Tifany"});
            animals.AddAnimal(new Horse() { CreatureName = "Alex" });
            animals.AddAnimal(new Dace() { CreatureName = "Tasha" });
            animals.AddAnimal(new Crucian() { CreatureName = "Walter" });
            animals.AddAnimal(new Crucian() { CreatureName = "Josh" });
            animals.AddAnimal(new Dog() { CreatureName = "Katrin" });
            animals.AddAnimal(new Horse() { CreatureName = "Ema" });

            animals.CalculateAnimalPaws();

            animals.ShowAnimalBreatheUnderwater();

            Console.ReadLine();
        }
    }
}
