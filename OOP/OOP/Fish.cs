﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    public abstract class Fish : AliveCreature, IBreathe
    {
        public void BreatheUnderWater()
        {
            Console.WriteLine("I am fish {0} and I can breathe underwater",CreatureName);
        }
    }
    public class Crucian : Fish, IEat
    {
        public Crucian()
        {
            CreatureName = "Crucian";
        }

        public Crucian(String crucianName)
        {
            CreatureName = crucianName;
        }
        public void EatingHay()
        {
            Console.WriteLine("I am unique crucian {0} , i can eat hay",CreatureName);
        }

        public override string ToString()
        {
            return String.Format("Crucian {0}", CreatureName);
        }
    }
    public class Dace : Fish
    {
        public Dace()
        {
            CreatureName = "Dace";
        }
        public Dace(String daceName)
        {
            this.CreatureName = daceName;
        }

        public override string ToString()
        {
            return String.Format("Dace {0}", CreatureName);
        }
    }
}
