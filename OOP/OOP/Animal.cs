﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    public abstract class Animal:AliveCreature
    {
    }
    public class Horse : Animal, IEat
    {
        public Horse()
        {
            CreatureName = "Horse";
        }

        public Horse(String horseName)
        {
            CreatureName = horseName;
        }
        public void EatingHay()
        {
            Console.WriteLine("I am Horse {0} and I like eat hay",CreatureName);
        }

        public override string ToString()
        {
            return String.Format("Horse {0}", CreatureName);
        }
    }

    public class Dog : Animal, IBreathe
    {
        public Dog()
        {
            CreatureName = "Dog";
        }

        public Dog(String dogName)
        {
            CreatureName = dogName;
        }
        public void BreatheUnderWater()
        {
            Console.WriteLine("I am unique dog {0} and I can breathe underwater",CreatureName);
        }

        public override string ToString()
        {
            return String.Format("Dog {0}", CreatureName);
        }
    }
}
