﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;

namespace WaterMarkTask
{
    public class WorkWithImage
    {
        public List<String> GetImageName(string path)
        {
            List<String> imageNames = new List<String>();

            List<string> extensions = new List<string>() { ".bmp", ".jpg", ".png", ".gif" };

            DirectoryInfo dir = new DirectoryInfo(path);

            foreach (FileInfo file in dir.GetFiles())
            {
                if (extensions.Contains(file.Extension))
                {
                    imageNames.Add(file.Name);
                }
            }
            imageNames.Remove("TempFile.jpg");
            return imageNames;
        }

        public String GetMD5RealAdmin()
        {
            string admin = "I_Am_Real_Admin";
            MD5 md5 = MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(admin);
            byte[] hash = md5.ComputeHash(inputBytes);
            admin = BitConverter.ToString(hash).Replace("-", "");
            return admin;
        }
    }
}