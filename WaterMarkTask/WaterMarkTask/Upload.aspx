﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Upload.aspx.cs" Inherits="WaterMarkTask.Upload" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            &nbsp;<asp:Label runat="server">Загрузить картинку на сервер</asp:Label>
            <br />
            <asp:FileUpload ID="FileUpload1" runat="server"  />
            <br />
            <asp:Label ID="Label1" runat="server" Text="Статус:"></asp:Label>
            <br />
            <asp:Button ID="ButtonUpload" OnClick="FileUpload2_OnClick" runat="server" Text="Загрузить картинку"/>
        </div>
    </form>
</body>
</html>
