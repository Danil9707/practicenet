﻿using System;
using System.IO;
using System.Web;

namespace WaterMarkTask
{
    public class Md5ModuleChecker : IHttpModule
    {
        #region IHttpModule Members

        private readonly WorkWithImage _getMd5 = new WorkWithImage();
        public void Dispose()
        {
            //удалите здесь код.
        }

        public void Init(HttpApplication context)
        {
            // Ниже приводится пример обработки события LogRequest и предоставляется 
            // настраиваемая реализация занесения данных
            context.LogRequest += new EventHandler(OnLogRequest);
            context.EndRequest += Context_BeginRequest;
            context.RequestCompleted += Context_RequestCompleted;
        }

        private void Context_RequestCompleted(object sender, EventArgs e)
        {
            var control = (HttpApplication)sender;
            var path = control.Server.MapPath("~/Images/" + "TempFile" + ".jpg");
            if (File.Exists(path))
                File.Delete(path);
        }

        private void Context_BeginRequest(object sender, EventArgs e)
        {
            var control = (HttpApplication)sender;
            var key = control.Request.QueryString["UserKey"];
            if (!String.IsNullOrEmpty(key))
            {
                if (key.ToLower() != _getMd5.GetMD5RealAdmin().ToLower())
                {
                    control.Response.Clear();
                    control.Response.Write("You are not autorizade");
                }
            }
            else
            {
                control.Response.Clear();
                control.Response.Write("You are not autorizade");

            }
        }

        #endregion

        public void OnLogRequest(Object source, EventArgs e)
        {
            //здесь можно разместить логику занесения данных
        }
    }
}
