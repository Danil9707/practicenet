﻿using System;
using System.IO;

namespace WaterMarkTask
{
    public partial class Home : System.Web.UI.Page
    {
        private readonly WorkWithImage _image = new WorkWithImage();
        private String _path;
        protected void Page_Init(object sender, EventArgs e)
        {
            _path = Server.MapPath("~/Images/");
            ImageList.DataSource = _image.GetImageName(_path);
            ImageList.DataBind();

        }
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void WaterMark(object sender, EventArgs e)
        {
            _path = "ImageHandler.ashx?ImageName=" + ImageList.SelectedItem.Value + "&UserKey=" + _image.GetMD5RealAdmin();
            Response.Redirect(_path);
        }
    }
}