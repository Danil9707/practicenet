﻿using System.Drawing;
using System.IO;
using System.Web;
using System.Web.Configuration;

namespace WaterMarkTask
{
    /// <summary>
    /// Сводное описание для ImageHandler1
    /// </summary>
    public class ImageHandler1 : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            
            string fileName = context.Request.QueryString["ImageName"];
            context.Response.ContentType = "image/jpg";
            using (var image = Image.FromFile(context.Server.MapPath("~/Images/" + fileName)))
            {
                using (var graphics = Graphics.FromImage(image))
                {
                    var textBounds = graphics.VisibleClipBounds;
                    textBounds.Inflate(-5, -5);
                    Font font = new Font("Arial", 32, FontStyle.Bold);
                    graphics.DrawString(
                         WebConfigurationManager.AppSettings["WaterMarkText"],
                        font,
                        Brushes.Red,
                        textBounds
                        );
                  
                    image.Save(context.Server.MapPath("~/Images/" + "TempFile" + ".jpg"));
                    //context.Response.OutputStream.Write(File.ReadAllBytes(context.Server.MapPath("~/Images/" + fileName)),
                    //    0, File.ReadAllBytes(context.Server.MapPath("~/Images/" + fileName)).Length);
                    //context.Response.End();
                    context.Response.WriteFile(context.Server.MapPath("~/Images/" + "TempFile" + ".jpg"));
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}