﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;
using ControlSample;

namespace ControlLibrary
{
    public class MyCompositeControl : CompositeControl, INamingContainer
    {
        #region Properties and Fields
        public static QuestionCollection Collection { get; set; }
        public static Boolean FirstResponse { get; set; }
        public Int32 QuestionSize
        {
            set { _question.Font.Size = FontUnit.Point(value); }
        }

        public Int32 AnswerSize
        {
            set { _answerListView.Font.Size = FontUnit.Point(value); }
        }

        private Label _question = new Label();
        private DropDownList _questionList = new DropDownList();

        private BulletedList _answerListView = new BulletedList();
        private DropDownList _answerList = new DropDownList();

        private Button _response = new Button();
        private Button _testEnd = new Button();

        private Panel _panel = new Panel();
        private Label _commonQuestionCount = new Label();
        private Label _correctQuestionCount = new Label();
        private Label _incorrectQuestionCount = new Label();

        #endregion

        #region Events
        public event EventHandler QuestionClick;
        public event EventHandler TestEnd;
        #endregion
        #region Ovverride methods
        protected override void CreateChildControls()
        {
            if (DesignMode) // Если дизайнер Visual Studio
            {
                Label label = new Label();
                label.Text = "[MyCompositeControl]";
                Controls.Add(label);
            }
            else // Если проект выполняется
            {
                CreateControls();
            }

            base.CreateChildControls();
        }

        #endregion
        private void CreateControls()
        {
            _questionList.AutoPostBack = true;
            _questionList.SelectedIndexChanged += _questionList_SelectedIndexChanged;
            InitializeQuestionList();

            _answerListView.BulletStyle = BulletStyle.Numbered;
            _answerList.AutoPostBack = true;

            if (FirstResponse)
            {
                var item = Collection[0];
                _question.Text = item.QuestionContent;
                SetList(item);
            }

            _response.Text = "Ответить";
            _response.Click += _response_Click;
            _testEnd.Text = "Завершить тест";
            _testEnd.Click += _testEnd_Click;
            QuestionClick += MyCompositeControl_QuestionClick;
            TestEnd += MyCompositeControl_TestEnd;
            _panel.BorderStyle = BorderStyle.Dashed;


            InitializePanel();

            Controls.Add(new LiteralControl("Все вопросы <br />"));
            Controls.Add(_questionList);
            Controls.Add(new LiteralControl("<br /> Выбранный вопрос <br />"));
            Controls.Add(_question);
            Controls.Add(new LiteralControl("<br />Список ответов <br />"));
            Controls.Add(_answerListView);
            Controls.Add(new LiteralControl("<br />Выберите  <br />"));
            Controls.Add(_answerList);
            Controls.Add(new LiteralControl("<br />"));
            Controls.Add(_response);
            Controls.Add(_testEnd);
            Controls.Add(_panel);

            _panel.Controls.Add(new LiteralControl("Общее кол-во вопросов: "));
            _panel.Controls.Add(_commonQuestionCount);
            _panel.Controls.Add(new LiteralControl("<br />Всего верных ответов: "));
            _panel.Controls.Add(_correctQuestionCount);
            _panel.Controls.Add(new LiteralControl("<br />Всего не верных ответов: "));
            _panel.Controls.Add(_incorrectQuestionCount);

        }
        private void TestEndLogic()
        {
            if (!CheckEnd())
            {
                Page.Response.Redirect(
                    $"Тест завершён, Ваши результаты {Collection.TrueCountResponse} правильных из {Collection.CommonCountQuestion}");
            }
            Page.Response.Write("Вы не ответили на все вопросы");
        }
        private void MyCompositeControl_TestEnd(object sender, EventArgs e)
        {
            TestEndLogic();
        }

        private void QuestionClickLogic()
        {
            if (!CheckEnd())
            {
                return;
            }
            var item = Collection[Convert.ToInt32(_questionList.SelectedValue) - 1];
            if (!CheckAnswer(item))
            {
                return;
            }

            SetItemAnswer(item);

            item.IsAnwered = true;
            SetResult(item);
            if (_questionList.Items.Count > _questionList.SelectedIndex + 1)
            {
                _questionList.SelectedIndex = _questionList.SelectedIndex + 1;

            }
            else
            {
                _questionList.SelectedIndex = 0;
            }
            _questionList_SelectedIndexChanged(_questionList, null);
        }
        private void MyCompositeControl_QuestionClick(object sender, EventArgs e)
        {
            QuestionClickLogic();
        }

        #region Help methods
        private void InitializeQuestionList()
        {
            for (var i = 1; i <= Collection.CommonCountQuestion; i++)
            {
                _questionList.Items.Add(i.ToString());
            }
        }

        private void InitializePanel()
        {
            _commonQuestionCount.Text = Collection.CommonCountQuestion.ToString();
            _correctQuestionCount.Text = Collection.TrueCountResponse.ToString();
            _incorrectQuestionCount.Text = Collection.FalseCountResponse.ToString();
        }
        private bool CheckEnd()
        {
            if (Collection.CommonCountQuestion == Collection.FalseCountResponse + Collection.TrueCountResponse)
            {
                Page.Response.Write("Вы завершили тест");
                string str =
                    $"<br/> Ваши результаты {Collection.TrueCountResponse} правильных из {Collection.CommonCountQuestion}";

                Page.Response.Write(str);

                return false;
            }
            return true;
        }

        private bool CheckAnswer(Question item)
        {
            if (item.IsAnwered)
            {
                Page.Response.Write("Вы уже ответили на этот вопрос, перейдите к следующему");
                return false;
            }
            return true;
        }

        private void SetItemAnswer(Question item)
        {
            switch (_answerList.SelectedIndex + 1)
            {
                case 1:
                    {
                        item.Selected = CorrectAnswer.First;
                        break;
                    }
                case 2:
                    {
                        item.Selected = CorrectAnswer.Second;
                        break;
                    }
                case 3:
                    {
                        item.Selected = CorrectAnswer.Third;
                        break;
                    }
                case 4:
                    {
                        item.Selected = CorrectAnswer.Fourth;
                        break;
                    }

            }
        }

        private void SetResult(Question item)
        {
            if (_answerList.SelectedIndex + 1 == (Int32)item.CorrectAnswer)
            {
                Collection.TrueCountResponse++;
                _correctQuestionCount.Text = Collection.TrueCountResponse.ToString();
            }
            else
            {
                Collection.FalseCountResponse++;
                _incorrectQuestionCount.Text = Collection.FalseCountResponse.ToString();
            }
        }
        private void SetList(Question item)
        {
            _answerListView.Items.Clear();
            _answerListView.Items.Add(item.FirstRequest);
            _answerListView.Items.Add(item.SecondRequest);
            _answerListView.Items.Add(item.ThirdRequest);
            _answerListView.Items.Add(item.FourthRequest);
            if (item.IsAnwered)
            {
                if (item.Selected == item.CorrectAnswer)
                {
                    _answerListView.Items[(Int32)item.Selected - 1].Attributes.Add("style", "color:green");
                }
                else
                {
                    _answerListView.Items[(Int32)item.Selected - 1].Attributes.Add("style", "color:red");
                }
            }

            _answerList.Items.Clear();
            _answerList.Items.Add("1");
            _answerList.Items.Add("2");
            _answerList.Items.Add("3");
            _answerList.Items.Add("4");
        }
        #endregion

        #region Event Handlers
        private void _testEnd_Click(object sender, EventArgs e)
        {
            TestEnd.Invoke(sender, e);
        }


        private void _response_Click(object sender, EventArgs e)
        {
            QuestionClick.Invoke(sender, e);
        }


        private void _questionList_SelectedIndexChanged(object sender, EventArgs e)
        {
            var item = Collection[Convert.ToInt32(_questionList.SelectedValue) - 1];
            _question.Text = item.QuestionContent;

            SetList(item);

        }
        #endregion

    }
}
