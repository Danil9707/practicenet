﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlSample
{
    public enum CorrectAnswer
    {
        First = 1,
        Second = 2,
        Third = 3,
        Fourth = 4
    }
    public class Question
    {
        public CorrectAnswer CorrectAnswer;
        public CorrectAnswer Selected;
        public String QuestionContent { get; set; }
        public String FirstRequest { get; set; }
        public String SecondRequest { get; set; }
        public String ThirdRequest { get; set; }
        public String FourthRequest { get; set; }

        public Boolean IsAnwered = false;

        private Question()
        {

        }

        public static Question GetNewTest(String question, String First, String Second, String Third, String Fourth, CorrectAnswer correct)
        {
            return new Question
            {
                QuestionContent = question,
                FirstRequest = First,
                SecondRequest = Second,
                FourthRequest = Fourth,
                ThirdRequest = Third,
                CorrectAnswer = correct,
                IsAnwered = false
            };
        }
    }

    public class QuestionCollection
    {
        private List<Question> _question;
        public Int32 CommonCountQuestion { get; private set; }
        public Int32 FalseCountResponse { get; set; }
        public Int32 TrueCountResponse { get; set; }

        public QuestionCollection(List<Question> list)
        {
            _question = list;
            CommonCountQuestion = list.Count;
            FalseCountResponse = 0;
            TrueCountResponse = 0;
        }

        public Question this[int index]
        {
            get { return _question[index]; }
        }

        public void AddQuestion(Question question)
        {
            _question.Add(question);
        }
    }
}
