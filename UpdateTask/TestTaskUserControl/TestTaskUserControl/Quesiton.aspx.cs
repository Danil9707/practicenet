﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ControlLibrary;
using ControlSample;

namespace TestTaskUserControl
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        private static List<Question> test = new List<Question>
                {
                    Question.GetNewTest("Чему равно чилсо Pi?","3.14", "2.14", "14.3", "41", CorrectAnswer.First),
                    Question.GetNewTest("Первая столица Украины?","Киев", "Харьков", "Москва", "Салтовка", CorrectAnswer.Second),
                    Question.GetNewTest("Кто создал C#?","Андерсон Хелсберг", "Билл Гейтс", "Стив Джобс", "Майк Тайсон", CorrectAnswer.First),
                    Question.GetNewTest("Сколько спутников у Земли?","1", "2", "3", "10", CorrectAnswer.First),
                    Question.GetNewTest("Самая большая по численности населения страна ?","Индия", "Китай", "США", "Россия", CorrectAnswer.Second),
                    Question.GetNewTest("Кто такой Neymar ?","Пeвец", "Футболист", "Баскетболист", "Политик", CorrectAnswer.Third),
                    Question.GetNewTest("Сколько спутников у Юпитера?","67", "52", "34", "102", CorrectAnswer.First)
                    

        };

        private static QuestionCollection collection = new QuestionCollection(test);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MyCompositeControl.Collection = collection;
                MyCompositeControl.FirstResponse = true;
            }
            else
            {
                MyCompositeControl.FirstResponse = false;
            }

        }
    }
}