﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Web.Administration;

namespace DeployingSite
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ServerManager serverManager = new ServerManager())
            {
                try
                {
                    string physicalPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "CounterWebApp");
                    ApplicationPool pool = serverManager.ApplicationPools.Add("CountersWebPool");
                    Site site = serverManager.Sites.Add("CountersSite", physicalPath, 8991);
                    site.ServerAutoStart = true;
                    site.Applications.First().ApplicationPoolName = pool.Name;
                    pool.ManagedPipelineMode = ManagedPipelineMode.Integrated;
                    serverManager.CommitChanges();
                }
                catch (COMException)
                {
                    Console.WriteLine("Site or pool with such names has been created. Check entered names out and try again.");
                }
            }
        }
    }
}
