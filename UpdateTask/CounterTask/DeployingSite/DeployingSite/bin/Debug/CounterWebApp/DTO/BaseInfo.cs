﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApplicationLifeCycle.DTO
{
    public class BaseInfo
    {
        public Int32 Count { get; set; }
        public DateTime  Time { get; set; }
    }
}