﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using ApplicationLifeCycle.XML;

namespace ApplicationLifeCycle
{
    public class Global : System.Web.HttpApplication
    {

        public static XMLWorker xml = new XMLWorker();
        void Application_Start(object sender, EventArgs e)
        {
            Application.Lock();
            xml.UpdateCountRequest();
            xml.UpdateCountViwers();
            Application.UnLock();
        }


        void Application_Error(object sender, EventArgs e)
        {
            Debug.WriteLine("Error");
            Exception ex = Server.GetLastError().GetBaseException();
        }


        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            Application.Lock();
            xml.AddDayRequest();
            xml.UpdateCountRequest();
            Application.Lock();
        }
    }
}
