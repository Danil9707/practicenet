﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Xml.Linq;

namespace ApplicationLifeCycle.XML
{
    public class XMLWorker
    {
        private XDocument _doc;
        private String _path;
        

        public XMLWorker()
        {
            _path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, 
                WebConfigurationManager.AppSettings["NameOfXmlFile"]);
            if (!File.Exists(_path))
                CreateDocument();
        }

        private void CreateDocument()
        {
            _doc = new XDocument(
                new XDeclaration("1.0", "utf-8", "yes"),
                new XElement("ApplicationInfo",
                    new XElement("CommonInfo",
                        new XElement("ViewersInfo",
                                new XElement("ViewerIP",
                                    new XElement("Value", "192.168.0.1")),
                                new XElement("CommonView",
                                    new XElement("Value", 0)),
                                new XElement("UniqueView",
                                    new XElement("Value", 0))
                                    )
                                    ,
                        new XElement("RequestsInfo",
                                new XElement("CommonRequest",
                                    new XElement("Value", 0)),
                                new XElement("DayRequest",
                                    new XElement("Count", 0),
                                    new XElement("Date", DateTime.Now.ToShortDateString())))
                ),

                    new XElement("PageInfo",
                        new XElement("Default",
                          new XElement("CountRequest", 0)),
                        new XElement("FirstPage",
                          new XElement("CountRequest", 0)),
                        new XElement("SecondPage",
                          new XElement("CountRequest", 0)))
                    ));
            _doc.Save(_path);
        }

        private void CommonUpdator(String node, String value)
        {
            try
            {
                _doc = XDocument.Load(_path);
                var item = Convert.ToInt32(_doc.Descendants(node)?.Descendants(value)?.FirstOrDefault()?.Value);
                item++;
                _doc.Descendants(node).Descendants(value).FirstOrDefault().Value = item.ToString();
                _doc.Save(_path);
            }
            catch (Exception ex)
            {

                Debug.WriteLine(ex.ToString());
            }
           
        }

        public void UpdateCountViwers()
        {
            CommonUpdator("CommonView", "Value");
        }

        public void UpdateCountRequest()
        {
            CommonUpdator("CommonRequest", "Value");
        }

        public void UpdatePageRequsetCount(String pageName)
        {
            CommonUpdator(pageName, "CountRequest");
        }

        private bool IsUnique(string ip)
        {
            _doc = XDocument.Load(_path);
            foreach (var item in _doc.Descendants("ViewerIP").Descendants("Value"))
            {
                if (item.Value == ip)
                {
                    return false;
                }
            }
            return true;
        }

        public void AddDayRequest()
        {
            _doc = XDocument.Load(_path);
            if (_doc.Descendants("DayRequest").Descendants("Date").FirstOrDefault().Value
                == DateTime.Now.ToShortDateString())
            {
                CommonUpdator("DayRequest", "Count");
            }
            else
            {
                _doc.Descendants("DayRequest").Descendants("Count").FirstOrDefault().Value = "0";
               _doc.Descendants("DayRequest").Descendants("Date").FirstOrDefault().Value =
                    DateTime.Now.ToShortDateString();
                _doc.Save(_path);
            }

        }
        private void AddIp(string ip)
        {
                _doc.Descendants("ViewerIP").FirstOrDefault().
                    Add(new XElement("Value", ip));
            _doc.Save(_path);
        }
        public void AddUnique(string ip)
        {
            if (IsUnique(ip))
            {
                AddIp(ip);
                CommonUpdator("UniqueView", "Value");
            }
        }
    }
}