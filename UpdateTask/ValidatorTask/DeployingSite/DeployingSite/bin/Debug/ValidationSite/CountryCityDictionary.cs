﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ValidatorTask
{
    public class CountryCityDictionary
    {
        private Dictionary<String, List<String>> country = new Dictionary<string, List<string>>();

        public CountryCityDictionary()
        {
            country.Add("Украина", new List<string> {"Харьков", "Донецк", "Днепропетровск"});
            country.Add("Россия", new List<string> { "Москва", "Питер", "Хабаровск" });
            country.Add("США", new List<string> { "Нью-Йорк", "Лос Анджелес", "Сан Франциско" });
            country.Add("Франция", new List<string> { "Париж", "Лион", "Марсель" });
        }

        public List<string> GetCity(string countryName)
        {
            return country[countryName];
        }
        public List<String> GetCounty()
        {
            return country.Keys.ToList();
        }
        
    }
}