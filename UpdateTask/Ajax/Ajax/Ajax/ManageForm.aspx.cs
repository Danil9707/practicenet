﻿using Ajax.Entity;
using Ajax.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ajax
{
    public partial class ManageForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager.ScriptResourceMapping.AddDefinition("jquery", new ScriptResourceDefinition
            {
                Path = "~/scripts/jquery-1.7.2.min.js",

            });
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            CreationControl.CreateRequiredFieldValidator("RequiredFieldName", "I_Name", "Заполните поле Имя товара",form1);
            CreationControl.CreateRequiredFieldValidator("RequiredFieldPrice", "I_Price", "Заполните поле Цена товара", form1);
            CreationControl.CreateComparerValidator("CheckPrice","Цена не соответствует формату 0000,0000", "I_Price", form1);
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(I_Name.Text) && String.IsNullOrEmpty(I_Price.Text))
                {
                    return;
                }
                if (FileUpload1.HasFile)
                {
                    string fileExtension = System.IO.Path.GetExtension(FileUpload1.FileName);
                    if (fileExtension != null && fileExtension.ToLower() != ".jpg")
                    {
                        Page.Response.Write("Стату: Выберите выерный формат");
                        return;
                    }
                    else
                    {
                        string newName = Guid.NewGuid().ToString();
                        FileUpload1.SaveAs(Server.MapPath("~/Images/" + newName + ".jpg"));
                        using (var item = new ItemsPriceEntities())
                        {
                            item.Items.Add(new Item
                            {
                                ItemName = I_Name.Text,
                                ItemPhoto = newName + ".jpg",
                                ItemPrice = Convert.ToDecimal(I_Price.Text)
                            });
                            item.SaveChanges();
                            WorkWithEntity._itemCollection = item.Items.ToList();
                        }
                        Page.Response.Redirect(Request.Url.AbsoluteUri);
                        Page.Response.Write("Стату: Товар успешно добавлен");
                        
                    }

                }
            }
            catch (Exception exp)
            {
                Page.Response.Write(exp);
            }

        }

    }
}