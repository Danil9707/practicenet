﻿using Ajax.Entity;
using Ajax.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ajax
{
    public partial class TestControl : System.Web.UI.Page
    {
        public static List<List<Item>> ItemListCollection = new List<List<Item>>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ItemListCollection = new List<List<Item>>();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            var temp = WorkWithEntity.GetElement();

            if (temp == null)
            {
                Button1.Enabled = false;
                UpdatePanel1.ContentTemplateContainer.Controls.Add(new Literal { Text = "<h3>Товары закончились<h3>" });
                UpdatePanel1.ContentTemplateContainer.Controls.Add(new Literal { Text = "<br/> " });
                WorkWithEntity.Pointer = 0;
            }
            else
            {
                ItemListCollection.Add(temp);
            }

            foreach (var item in ItemListCollection)
            {
                foreach (var element in item)
                {
                    Panel pn = new Panel();
                    pn.CssClass = "panel";

                    Label name = new Label();
                    name.Text = "Car Model: " + element.ItemName;
                    name.CssClass = "label";

                    Label price = new Label();
                    price.Text = "Car Price:" + element.ItemPrice.ToString() + "$";
                    price.CssClass = "label";

                    Image img = new Image();
                    img.CssClass = "image";
                    img.ImageUrl = ("~/Images/" + element.ItemPhoto);

                    pn.Controls.Add(img);
                    pn.Controls.Add(name);
                    pn.Controls.Add(price);


                    UpdatePanel1.ContentTemplateContainer.Controls.Add(pn);
                }
            }






        }
    }
}