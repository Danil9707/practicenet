﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ItemsView.aspx.cs" Inherits="Ajax.TestControl" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
        .panel {
            display: inline-block;
            Width: 30%;
            height: 250px;
            padding: 15px;
        }

        .image {
            width: 100%;
            height: 350px;
        }

        .label {
            display: block;
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            font-size: large;
            padding: 5px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>

            <asp:UpdatePanel ID="UpdatePanel1" runat="server">

                <ContentTemplate>
                    <br />
                    <asp:Button ID="Button1" runat="server" Text="Show Items" OnClick="Button1_Click" />
                    <br />
                </ContentTemplate>

            </asp:UpdatePanel>
            <a href="HomePanel.aspx">Возврат домой</a>
        </div>
    </form>
</body>
</html>
