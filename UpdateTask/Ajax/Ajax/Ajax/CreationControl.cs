﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Ajax
{
    public static class CreationControl
    {
        #region Create Validator

        public static void CreateRequiredFieldValidator(string id, string targetElement, string errorMessage,
            HtmlForm form1)
        {
            RequiredFieldValidator field = new RequiredFieldValidator
            {
                ID = id,
                ControlToValidate = targetElement,
                ErrorMessage = errorMessage,
                Display = ValidatorDisplay.None
            };
            form1.Controls.Add(field);
        }

      

        public static void CreateComparerValidator(string id, string errorMessage, string targetElement,
            HtmlForm form1)
        {
            var validator = new CompareValidator
            {
                ID = id,
                ErrorMessage = errorMessage,
                ControlToValidate = targetElement,
                Type = ValidationDataType.Double,
                Operator = ValidationCompareOperator.DataTypeCheck,
                Display = ValidatorDisplay.None
            };
            form1.Controls.Add(validator);
        }

        #endregion
    }
}