﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageForm.aspx.cs" Inherits="Ajax.ManageForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:GridView ID="GridView1" runat="server" DataSourceID="EntityDataSource1" AllowPaging="True" CellPadding="4" ForeColor="#333333" GridLines="None" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="ItemId">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:BoundField DataField="ItemId" HeaderText="ItemId" ReadOnly="True" SortExpression="ItemId" />
                    <asp:BoundField DataField="ItemName" HeaderText="ItemName" SortExpression="ItemName" />
                    <asp:BoundField DataField="ItemPrice" HeaderText="ItemPrice" SortExpression="ItemPrice" />
                    <asp:CommandField ButtonType="Button" ShowDeleteButton="True" ShowEditButton="True" />
                </Columns>
                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                <SortedAscendingCellStyle BackColor="#FDF5AC" />
                <SortedAscendingHeaderStyle BackColor="#4D0000" />
                <SortedDescendingCellStyle BackColor="#FCF6C0" />
                <SortedDescendingHeaderStyle BackColor="#820000" />
            </asp:GridView>
            <asp:EntityDataSource ID="EntityDataSource1" runat="server" ConnectionString="name=ItemsPriceEntities"
                DefaultContainerName="ItemsPriceEntities" EnableFlattening="False" EntitySetName="Items" EnableDelete="True" EnableInsert="True" EnableUpdate="True">
            </asp:EntityDataSource>
        </div>
        <asp:Panel ID="Panel1" runat="server" Width="33%">
            Имя товара<br />
            <asp:TextBox ID="I_Name" runat="server"></asp:TextBox>
            <br />
            Цена товара<br />
            <asp:TextBox ID="I_Price" runat="server"></asp:TextBox>
            <br />
            Фото файла<br />
            <asp:FileUpload ID="FileUpload1" runat="server" />
            <br />
            <br />
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Добавить товар" />
        </asp:Panel>
        <asp:ValidationSummary ID="ValidationSummary1" DisplayMode="BulletList" runat="server" />
        <a href="HomePanel.aspx">Возврат домой</a>
    </form>
</body>
</html>
