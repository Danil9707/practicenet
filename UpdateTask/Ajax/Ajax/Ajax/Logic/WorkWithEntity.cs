﻿using Ajax.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ajax.Logic
{
    public static class WorkWithEntity
    {
        public static List<Item> _itemCollection;
        public static Int32 Pointer = 0;
        static WorkWithEntity()
        {
            using (var item = new ItemsPriceEntities())
            {
                _itemCollection = item.Items.ToList();
            }
        }

        public static List<Item> GetElement()
        {
            try
            {
                var item = new List<Item>();
                if (Pointer + 2 < _itemCollection.Count)
                {

                    var IndexOfLastReturnElement = Pointer + 2;
                    for (int i = Pointer; i <= IndexOfLastReturnElement; i++)
                    {
                        item.Add(_itemCollection[i]);
                    }
                    Pointer += 3;
                    return item;
                }
                int balanceElement = _itemCollection.Count - Pointer;
                if (balanceElement > 0)
                {
                    var IndexOfLastReturnElement = Pointer + balanceElement;
                    for (int i = Pointer; i < IndexOfLastReturnElement; i++)
                    {
                        item.Add(_itemCollection[i]);
                    }
                    Pointer = _itemCollection.Count;
                    return item;
                }
            }
            catch (Exception exp)
            {
                Console.Write("exp {0}",exp);
            }
           
            return null;
        }
    }
}