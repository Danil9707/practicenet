﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.IO;
using System.Linq;
using System.Resources;

namespace WorkWithResourses
{
    public class ResoursesView
    {
        public String Key { get; set; }
        public String Value { get; set; }
        public String Comment { get; set; }

        public ResoursesView()
        {

        }
    }

    public class ResoursesWorker
    {
        public static List<ResoursesView> List = new List<ResoursesView>();

        private static String _resxPath;
        public static string ResxFilePath
        {
            get { return _resxPath; }
            set
            {
                _resxPath = value;
                ReadResourses();
            }
        }
        private static ResXResourceReader _reader;
        private static ResXResourceWriter _writer;

        public ResoursesWorker()
        {
           
        }
        private static void UpdateResoursesFile()
        {
            try
            {
                using (_writer = new ResXResourceWriter(ResxFilePath))
                {
                    foreach (var resoursesView in List)
                    {
                        ResXDataNode node =
                            new ResXDataNode(resoursesView.Key, resoursesView.Value);
                        node.Comment = resoursesView.Comment;
                        _writer.AddResource(node);
                    }

                }


            }
            catch (Exception exp)
            {
                Console.WriteLine(exp);
            }

        }

        private static void ReadResourses()
        {
            List = new List<ResoursesView>();
            using (_reader = new ResXResourceReader(ResxFilePath))
            {
                _reader.UseResXDataNodes = true;
                foreach (DictionaryEntry entry in _reader)
                {
                    List.Add(ShowResourceItem(entry));
                }
            }
        }
        private static ResoursesView ShowResourceItem(DictionaryEntry entry)
        {
            // Use a null type resolver.
            ITypeResolutionService typeres = null;
            ResXDataNode dnode;

            // Display from node info.
            dnode = (ResXDataNode)entry.Value;

            return new ResoursesView
            {
                Key = dnode.Name,
                Value = dnode.GetValue(typeres).ToString(),
                Comment = dnode.Comment
            };

        }
        public static void Add(ResoursesView res)
        {
            List.Add(res);
            UpdateResoursesFile();

        }

        public static void Delete(ResoursesView res)
        {
            if (res.Key == null)
            {
                return;
            }
            foreach (var item in List)
            {
                if (item.Key == res.Key)
                {
                    List.Remove(item);
                    break;
                }
            }
            UpdateResoursesFile();

        }

        public static bool CheckUniq(string key)
        {
            return List.All(item => item.Key != key);
        }

        public static void Update(ResoursesView res)
        {
            foreach (var item in List)
            {
                if (item.Key == res.Key)
                {
                    item.Value = res.Value;
                    item.Comment = res.Comment;
                    break;
                }
            }
            UpdateResoursesFile();
        }
        public List<ResoursesView> GetSelectedFileResourses()
        {
            return List;
        }
    }
}