﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace WorkWithResourses
{
    public class FindFile
    {
        public readonly List<String> FilePath;

        public FindFile(string direcrotyPath)
        {
            DirectoryInfo dr = new DirectoryInfo(direcrotyPath);
            FilePath = new List<string>();
            foreach (FileInfo file in dr.GetFiles())
            {
                if (file.Extension.ToLower() == ".resx")
                {
                    FilePath.Add(file.Name);
                }
            }
        }
    }
}