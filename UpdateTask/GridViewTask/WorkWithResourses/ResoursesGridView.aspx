﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResoursesGridView.aspx.cs" Inherits="WorkWithResourses.Work" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            Выберите ресурсный файл, который хотите редактировать 
        <br />
            <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged"></asp:DropDownList>
            <br />
            <asp:GridView ID="GridView1" runat="server" DataSourceID="ResoursesData" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" ShowFooter="True" OnRowDeleting="GridView1_OnRowDeleting" AllowPaging="True" Width="100%" PageSize="20">
                <Columns>
                    <asp:TemplateField HeaderText="Key" SortExpression="Key">
                        <EditItemTemplate>
                            <asp:TextBox ID="EditResoursesID" runat="server" Text='<%# Bind("Key") %>'></asp:TextBox>
                      <%--      <asp:CustomValidator ID="UniqueCheck" runat="server" Text ="*" ErrorMessage="Key Must Be Unique" ControlToValidate="EditResoursesID" OnServerValidate="UniqueCheck_OnServerValidate"></asp:CustomValidator>--%>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Key") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="ResoursesId" Width="95%" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Поле ключа должно быть заполнено" ControlToValidate="ResoursesId" ValidationGroup="AddNew" ForeColor="Red" Text="*"></asp:RequiredFieldValidator>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Value" SortExpression="Value">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Value") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("Value") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="ResoursesValue" Width="97%" runat="server"></asp:TextBox>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Comment" SortExpression="Comment">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Comment") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("Comment") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="ResoursesComment" Width="97%" runat="server"></asp:TextBox>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <EditItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" Text="Обновить"></asp:LinkButton>
                            &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Отмена"></asp:LinkButton>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit" Text="Правка"></asp:LinkButton>
                            &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Delete" Text="Удалить"></asp:LinkButton>
                        </ItemTemplate>
                        <FooterTemplate>

                            <asp:Button ID="AddNewRecord" ValidationGroup="AddNew" Width="97" Text="Add new record" runat="server" OnClick="AddNewRecord_OnClick" />
                        </FooterTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="White" ForeColor="#000066" />
                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                <RowStyle ForeColor="#000066" />
                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                <SortedDescendingHeaderStyle BackColor="#00547E" />
            </asp:GridView>
            <asp:ValidationSummary ForeColor="Red" ValidationGroup="AddNew"
                ID="ValidationSummary1" runat="server" />
            <asp:ObjectDataSource ID="ResoursesData" runat="server" DataObjectTypeName="WorkWithResourses.ResoursesView" DeleteMethod="Delete" InsertMethod="Add" SelectMethod="GetSelectedFileResourses" TypeName="WorkWithResourses.ResoursesWorker" UpdateMethod="Update"></asp:ObjectDataSource>
        </div>
    </form>
</body>
</html>
