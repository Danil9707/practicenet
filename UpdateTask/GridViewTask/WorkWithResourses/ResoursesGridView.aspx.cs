﻿using System;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WorkWithResourses
{
    public partial class Work : System.Web.UI.Page
    {
        private static string _directory;
        private static Int32 _selectIndex = -1;
        protected void Page_Load(object sender, EventArgs e)
        {

            ScriptManager.ScriptResourceMapping.AddDefinition("jquery", new ScriptResourceDefinition
            {
                Path = "~/scripts/jquery-1.7.2.min.js",

            });
            if (!IsPostBack)
            {
                _directory =
                   System.Configuration.ConfigurationManager.AppSettings["SelectedDomain"];
                FindFile file =
                    new FindFile
                    (_directory);
                DropDownList1.DataSource = file.FilePath;
                if (file.FilePath.Count > 0)
                {
                    int index = 0;
                    if (_selectIndex != -1)
                    {
                        DropDownList1.SelectedIndex = _selectIndex;
                        index = _selectIndex;
                    }
                    ResoursesWorker.ResxFilePath =
                       Path.Combine(_directory, file.FilePath[index]);
                    GridView1.DataBind();
                    DropDownList1.DataBind();
                }
                else
                {
                    Page.Response.Write("<p3> В выбранной вами директории отсутствуют файлы с расширением .Resx<p3>");
                }
            }

        }

        protected void AddNewRecord_OnClick(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                var key =
                     ((TextBox)GridView1.FooterRow.FindControl("ResoursesId")).Text;
                var value =
                     ((TextBox)GridView1.FooterRow.FindControl("ResoursesValue")).Text;
                var comment =
                    ((TextBox)GridView1.FooterRow.FindControl("ResoursesComment")).Text;
                if (ResoursesWorker.CheckUniq(key))
                {
                    ResoursesWorker.Add(new ResoursesView { Key = key, Value = value, Comment = comment });

                    ((TextBox)GridView1.FooterRow.FindControl("ResoursesId")).Text = string.Empty;
                    ((TextBox)GridView1.FooterRow.FindControl("ResoursesValue")).Text = string.Empty;
                    ((TextBox)GridView1.FooterRow.FindControl("ResoursesComment")).Text = string.Empty;
                    Page.Response.Redirect(Request.Url.ToString());
                }
                else
                    Page.Response.Write("<p3 style='color: red'>ID it's unique field<p3>");

            }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResoursesWorker.ResxFilePath =
                Path.Combine(_directory, DropDownList1.SelectedValue);
            GridView1.DataBind();
            _selectIndex = DropDownList1.SelectedIndex;

        }

        protected void GridView1_OnRowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ResoursesWorker.
                Delete(ResoursesWorker.List[e.RowIndex]);
            Page.Response.Redirect(Request.Url.ToString());
        }

    }
}