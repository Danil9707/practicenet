﻿using System;

namespace WaterMarkTask
{
    public partial class Upload : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void FileUpload2_OnClick(object sender, EventArgs e)
        {
            
          
            if (FileUpload1.HasFile)
            {
               string fileExtension = System.IO.Path.GetExtension(FileUpload1.FileName);
                if (fileExtension != null && fileExtension.ToLower() != ".jpg")
                {
                    Label1.Text = "Стату: Выберите выерный формат";
                }
                else
                {
                    string newName = Guid.NewGuid().ToString();
                    ClientScript.RegisterStartupScript(GetType(), "Идентификатор вашего загруженного фото",
                        "alert('" + newName + "');", true);
                    FileUpload1.SaveAs(Server.MapPath("~/Images/" + newName + ".jpg"));

                    Label1.Text = "Стату: Изображение успешно загружено";
                }

            }
        }

    }
}
