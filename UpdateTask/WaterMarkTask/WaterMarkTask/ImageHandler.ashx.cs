﻿using System.Drawing;
using System.IO;
using System.Web;
using System.Web.Configuration;

namespace WaterMarkTask
{
    /// <summary>
    /// Сводное описание для ImageHandler1
    /// </summary>
    public class ImageHandler1 : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            
            
            string fileName = context.Request.QueryString["ImageName"];
            string filePath = context.Server.MapPath("~/Images/" + fileName);

            // context.Response.ContentType = "image/jpg";
            using (var image = Image.FromFile(filePath))
            {
                using (var graphics = Graphics.FromImage(image))
                {
                    var textBounds = graphics.VisibleClipBounds;
                    textBounds.Inflate(-5, -5);
                    Font font = new Font("Arial", 32, FontStyle.Bold);
                    graphics.DrawString(
                         WebConfigurationManager.AppSettings["WaterMarkText"],
                        font,
                        Brushes.Red,
                        textBounds
                        );
                    filePath = context.Server.MapPath("~/Images/" + "TempFile" + ".jpg");
                    image.Save(filePath);

                    //context.Response.OutputStream.Write(File.ReadAllBytes(context.Server.MapPath("~/Images/" + fileName)),
                    //    0, File.ReadAllBytes(context.Server.MapPath("~/Images/" + fileName)).Length);
                    //context.Response.End();
                    // context.Response.WriteFile(context.Server.MapPath("~/Images/" + "TempFile" + ".jpg"));
                    context.Response.Write(" <img src='" + "/Images/TempFile.jpg" + "'/> <br />  <a href='Home.aspx'>Назад</a>");
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}