﻿using System;
using System.IO;
using System.Web;

namespace WaterMarkTask
{
    public class Md5ModuleChecker : IHttpModule
    {
        #region IHttpModule Members

        private readonly WorkWithImage _getMd5 = new WorkWithImage();
        public void Dispose()
        {
        }

        public void Init(HttpApplication context)
        {
            context.EndRequest += Context_BeginRequest;
        }

    

        private void Context_BeginRequest(object sender, EventArgs e)
        {
            var control = (HttpApplication)sender;
            var key = control.Request.QueryString["UserKey"];
            if (Home.successLogon)
            {
                return;
            }
            if (!String.IsNullOrEmpty(key))
            {
                if (key.ToLower() != _getMd5.GetMD5RealAdmin().ToLower())
                {
                    control.Response.Clear();
                    control.Response.Write("You are not autorizade");
                }
            }
            else
            {
                control.Response.Clear();
                control.Response.Write("You are not autorizade");

            }
        }

        #endregion
        
    }
}
