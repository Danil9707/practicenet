﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="WaterMarkTask.Home" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label runat="server">Ссылка на страницу загрузки</asp:Label>
            <br />
            <asp:HyperLink ID="HyperLink1" NavigateUrl="Upload.aspx" runat="server">Upload</asp:HyperLink>
            <br />
            <asp:Label runat="server">Список картинок</asp:Label>
            <br />
            <asp:DropDownList ID="ImageList" runat="server" AutoPostBack="True" />
            <br />
            <asp:Label runat="server">Загрузить на сервер</asp:Label>
            <br />
            <asp:Button runat="server" Text="Get my WaterMark" OnClick="WaterMark" />
            <br />
         
        </div>
    </form>
</body>
</html>
