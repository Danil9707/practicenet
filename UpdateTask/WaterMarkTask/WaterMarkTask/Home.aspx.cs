﻿using System;
using System.IO;

namespace WaterMarkTask
{
    public partial class Home : System.Web.UI.Page
    {
        private readonly WorkWithImage _image = new WorkWithImage();
        private String _path;
        public static Boolean successLogon = false;
        private readonly WorkWithImage _getMd5 = new WorkWithImage();


        protected void Page_Init(object sender, EventArgs e)
        {
            _path = Server.MapPath("~/Images/");
            ImageList.DataSource = _image.GetImageName(_path);
            ImageList.DataBind();

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            var key = Request.QueryString["UserKey"];
            if (!String.IsNullOrEmpty(key))
            {
                if (key.ToLower() == _getMd5.GetMD5RealAdmin().ToLower())
                {
                    successLogon = true;
                }
            }
            var path = Server.MapPath("~/Images/" + "TempFile" + ".jpg");
            if (File.Exists(path))
                File.Delete(path);

        }

        protected void WaterMark(object sender, EventArgs e)
        {
            _path = "ImageHandler.ashx?ImageName=" + ImageList.SelectedItem.Value + "&UserKey=" + _image.GetMD5RealAdmin();
           
            Response.Redirect(_path);
        }
    }
}