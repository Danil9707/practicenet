﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _02_PageLifeCycle
{
    public partial class Default : System.Web.UI.Page
    {
        // Событие используется для динамической установки MasterPage, смены темы.
        protected void Page_PreInit(object sender, EventArgs e)
        {
            Response.Write("Page_PreInit " + DateTime.Now);
            Response.Write("<br/>");

        }

        // Для чтения или инициализации свойств элементов управления.
        protected void Page_Init(object sender, EventArgs e)
        {
            Response.Write("Page_Init " + DateTime.Now);
            Response.Write("<br/>");

        }

        // Для действий, которые должны произвестись после всех инициализаций. До этого момента данные сохраненные во ViewState не сериализуються.
        protected void Page_InitComplete(object sender, EventArgs e)
        {
            Response.Write("Page_InitComplete " + DateTime.Now);
            Response.Write("<br/>");

        }

        // Для выполнения действий после загрузки ViewState и до события Load.
        protected void Page_PreLoad(object sender, EventArgs e)
        {
            Response.Write("Page_PreLoad " + DateTime.Now);
            Response.Write("<br/>");


        }

        // Используется для установки и инициализаций свойств открытия подключений к базам данных.
        //Page.Request.Form - представляет собой коллекцию, которая содержит ViewState всех элементов страницы
        //После каждой перезагрузки страницы , она запоминает состояние элементов и записывает их
        //ViewState Доступен только внутри одной страницы

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Write("Page_Load " + DateTime.Now);
            Response.Write("<br/>");
            Page.Response.Write("<table border='1'>");
            foreach (var item in Page.Request.Form.AllKeys)
            {
                Page.Response.Write("<tr>");
                foreach (var value in Page.Request.Form.GetValues(item))
                {
                    Page.Response.Write("<td>" +"Key:" + item +" Value:"+ value + "</td>");
                }
                
                Page.Response.Write("</tr>");

            }
            Page.Response.Write("</table>");

        }

        // Обработчик нажатия по кнопке.
        protected void Button_Click(object sender, EventArgs e)
        {
            Response.Write("Button_Click");
            Response.Write("<br/>");

        }

        // Для действий, которые должны происходить после выполнения обработчиков событий элементов управления.
        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            Response.Write("Page_LoadComplete " + DateTime.Now);
            Response.Write("<br/>");

        }

        // Для внесения окончательных изменений перед рендерингом страницы.
        protected void Page_PreRender(object sender, EventArgs e)
        {
            Response.Write("Page_PreRender " + DateTime.Now);
            Response.Write("<br/>");

        }

        // Используется при разработке асинхронных страниц.
        protected void Page_PreRenderComplete(object sender, EventArgs e)
        {
            Response.Write("Page_PreRenderComplete " + DateTime.Now);
            Response.Write("<br/>");

        }

        // Сохранение ViewState. Последнее событие перед рендерингом.
        protected void Page_SaveStateComplete(object sender, EventArgs e)
        {
            Response.Write("Page_SaveStateComplete " + DateTime.Now);
            Response.Write("<br/>");

        }

        // Рендеринг страницы.

        // Для освобождения ресурсов.
        protected void Page_Unload(object sender, EventArgs e)
        {
            //Response.Write("Page_Unload");
        }

    }
}