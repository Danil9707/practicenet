﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using Microsoft.Web.Administration;

namespace IISDeployer
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ServerManager serverManager = new ServerManager())
            {
                try
                {
                    string physicalPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PageLifeCycle");
                    ApplicationPool pool = serverManager.ApplicationPools.Add("PageLyfePool");
                    Site site = serverManager.Sites.Add("PageLifeSite", physicalPath, 8990);
                    site.ServerAutoStart = true;
                    site.Applications.First().ApplicationPoolName = pool.Name;
                    pool.ManagedPipelineMode = ManagedPipelineMode.Integrated;
                    serverManager.CommitChanges();
                }
                catch (COMException)
                {
                    Console.WriteLine("Site or pool with such names has been created. Check entered names out and try again.");
                }
            }
        }
    }
}
