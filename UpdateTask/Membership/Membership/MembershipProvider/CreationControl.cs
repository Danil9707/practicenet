﻿using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MembershipProviderSample
{
    public static class CreationControl
    {
        #region Create Validator

        public static RegularExpressionValidator CreateRegularValidator(string id, string targetControl,string message,string express)
        {
            RegularExpressionValidator reg = new RegularExpressionValidator
            {
                ID = id,
                ErrorMessage = message,
                ValidationExpression = express,
                Display = ValidatorDisplay.None,
                ControlToValidate = targetControl
            };
            return reg;
        }

        public static RequiredFieldValidator CreateRequiredFieldValidator(string id, string targetElement, string errorMessage)
        {
            RequiredFieldValidator field = new RequiredFieldValidator
            {
                ID = id,
                ControlToValidate = targetElement,
                ErrorMessage = errorMessage,
                Display = ValidatorDisplay.None
            };
            return field;
        }

     
        public static CompareValidator CreateComparerValidator(string id, string errorMessage, string targetElement,
            string compareElement)
        {
            var validator = new CompareValidator
            {
                ID = id,
                ErrorMessage = errorMessage,
                ControlToCompare = compareElement,
                ControlToValidate = targetElement,
                Operator = ValidationCompareOperator.Equal,
                Display = ValidatorDisplay.None
            };
            return validator;
        }


        #endregion
    }
}