﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Security;
using System.Web.UI.WebControls;

namespace MembershipProviderSample
{
    public partial class OperationWithUser : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

            UserChange.Visible = true;
            String user = GridView1.SelectedRow.Cells[1].Text;

            UserChange.UserName = user;
        }

    

        protected void deleteButton_Click(object sender, EventArgs e)
        {
            if (GridView1.SelectedRow == null)
            {
                Page.Response.Write("<h3>Для удаления сначало необходимо выбрать пользователя<h3>");
                return;
            }
            Membership.DeleteUser(GridView1.SelectedRow.Cells[1].Text);
            Page.Response.Redirect(Request.Url.AbsoluteUri);
        }
    }
}