﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserChangeControl.ascx.cs" Inherits="MembershipProviderSample.UserChangeControl" %>

Имя пользователя
<br />
<asp:TextBox runat="server" ID="UserNameTextBox" Enabled="false"  />

<br />
Пароль
<br />
<asp:TextBox runat="server" ID="PasswordTextBox" TextMode="Password" />

<br />
Подтверждение пароля
<br />
<asp:TextBox runat="server" ID="PasswordConfirmTextBox" TextMode="Password" />
<br />
Email
<br />
<asp:TextBox runat="server" ID="EmailTextBox" />

<asp:RegularExpressionValidator ID="RegularExpressionValidatorEmail" runat="server"
    ForeColor="Red" ErrorMessage="*" ControlToValidate="EmailTextBox" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
<asp:ValidationSummary ID="ValidationSummary1" DisplayMode="BulletList" runat="server" />
<br />
Изменить
<br />
<asp:Button ID="RegisterButton" Text="Register" runat="server" OnClick="RegisterButton_Click" />
  