﻿using System;
using System.Collections.Generic;
using System.Security;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MembershipProviderSample
{
    public partial class UserChangeControl : System.Web.UI.UserControl
    {
        public String UserName
        {
            set
            {
                UserNameTextBox.Text = value;
                EmailTextBox.Text = Membership.GetUser(value).Email;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager.ScriptResourceMapping.AddDefinition("jquery", new ScriptResourceDefinition
            {
                Path = "~/scripts/jquery-1.7.2.min.js",

            });
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            this.Controls.Add(CreationControl.CreateRequiredFieldValidator("RequiredUserName", "UserNameTextBox", "Заполните поле имя пользователя"));
            this.Controls.Add(CreationControl.CreateRequiredFieldValidator("RequiredEmail", "EmailTextBox", "Заполните поле Email"));
            this.Controls.Add(CreationControl.CreateRequiredFieldValidator("RequiredPassword", "PasswordTextBox", "Заполните поле Пароль"));
            this.Controls.Add(CreationControl.CreateComparerValidator("ComparePassword", "Пароли должны совпадать", "PasswordTextBox", "PasswordConfirmTextBox"));
            this.Controls.Add(CreationControl.CreateRegularValidator("RegularPassword", "PasswordTextBox",
                "Пароль должен быть от 6 до 20 символов(Буквы, числа, _).", @"[A-Za-z0-9_]{6-20}"));
            
        }

        protected void RegisterButton_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Membership.DeleteUser(UserNameTextBox.Text);
                Membership.CreateUser(UserNameTextBox.Text, PasswordTextBox.Text, EmailTextBox.Text);
                Page.Response.Redirect(Request.Url.AbsoluteUri);

            }

        }
    }
}