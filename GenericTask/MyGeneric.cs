﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericTask
{
    public class MyGeneric<T> : IComparable<T> where T : struct,IComparable<T>
    {
        public T MyType { get; set; }
        public int CompareTo(T other)
        {
            return MyType.CompareTo(other);
        }
        

        public override string ToString()
        {
            return MyType.ToString();
        }
        
    }
}
