﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenericTask;

namespace GenericTask
{

    class Program
    {
        static void PrintList<T>(List<T> list)
        {
            foreach (var item in list)
            {
                Console.WriteLine(item.ToString());
            }
        }
        static void Main(string[] args)
        {
            var testInt = new List<MyGeneric<Int32>>();
            Random rnd = new Random();
            for (int i = 0; i < 5; i++)
            {
                testInt.Add(new MyGeneric<Int32> { MyType = rnd.Next(0, 10) });
            }
            Console.WriteLine("List с типом {0} до сортировки ", testInt[0].MyType.GetType());
            PrintList(testInt);
            Console.WriteLine(new String('_', 80));

            testInt.Sort((x,y)=> x.MyType.CompareTo(y.MyType));
            
            Console.WriteLine("List с типом {0} после сортировки ", testInt[0].MyType.GetType());
            PrintList(testInt);


            Console.WriteLine(new String('_', 80));


            var testDouble = new List<MyGeneric<Double>>();
            for (int i = 0; i < 5; i++)
            {
                testDouble.Add(new MyGeneric<Double> { MyType = rnd.NextDouble() });
            }

            Console.WriteLine("List с типом {0} до сортировки ", testDouble[0].MyType.GetType());
            PrintList(testDouble);

            Console.WriteLine(new String('_', 80));

            testDouble.Sort((x, y) => x.MyType.CompareTo(y.MyType));
            Console.WriteLine("List с типом {0} после сортировки ", testDouble[0].MyType.GetType());
            PrintList(testDouble);

            Console.WriteLine(new String('_', 80));
            Console.ReadKey();
        }
    }
}
