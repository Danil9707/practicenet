﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace EstimateSpeed
{
    public class QualificationCollection
    {
        #region Fields and Properties

        public Int32 CountElement { get; set; }
        private ArrayList _testIntArrayList;
        private ArrayList _testStringArrayList;
        private List<Int32> _testIntList;
        private List<String> _testStringList;
        private Stopwatch TimeValue { get; set; }

        #endregion
        
        #region Help methods

        private void InitializeList()
        {
            _testIntArrayList = new ArrayList();
            _testStringArrayList = new ArrayList();
            _testIntList = new List<int>();
            _testStringList = new List<string>();
            TimeValue = new Stopwatch();
        }

        #endregion


        #region Constructor

        public QualificationCollection()
        {
            InitializeList();
            CountElement = 1000000;
        }

        public QualificationCollection(Int32 countElement)
        {
            InitializeList();
            CountElement = countElement;
        }

        #endregion

        #region Collection methods

        private void AddElementToCollection<T>(IList collection, T element)
        {
            TimeValue.Start();
            for (var i = 0; i < CountElement; i++)
            {
                collection.Add(element);
            }
            TimeValue.Stop();
        }

        private void SortArrayListCollection(ArrayList collection)
        {
            TimeValue.Start();
            collection.Sort();
            TimeValue.Stop();
        }

        private void SortListCollection<T>(List<T> collection)
        {
            TimeValue.Start();
            collection.Sort();
            TimeValue.Stop();
        }
        private void GetElementOfCollection<T>(IList collection)
        {
            T element;
            TimeValue.Start();
            foreach (var item in collection)
            {
                element = (T) item;
            }
            TimeValue.Stop();
        }

        #endregion

        #region Methods for Int32 in ArrayList 

        public void AddElementIntToArrayList()
        {
            
            AddElementToCollection(_testIntArrayList, 10);

            Console.WriteLine("Время затраченное на добавление в ArrayList {0} елементов  типа Int32 равно {1} мс", CountElement, TimeValue.ElapsedMilliseconds);
            TimeValue.Reset();
        }

        public void GetElementIntWithArrayList()
        {
            GetElementOfCollection<Int32>(_testIntArrayList);
            Console.WriteLine("Время затраченное на получение в ArrayList {0} елементов типа Int32 равно {1} мс", CountElement, TimeValue.ElapsedMilliseconds);
            TimeValue.Reset();
        }

        public void SortIntArrayList()
        {
            SortArrayListCollection(_testIntArrayList);
            Console.WriteLine("Время затраченное на сортировку ArrrayList с {0} елементами типа Int32 равно {1} мс", CountElement, TimeValue.ElapsedMilliseconds);
            TimeValue.Reset();
        }

        #endregion

        #region Methods for String in ArrayList

        public void AddElementStringToArrayList(string value = "Abc")
        {
            AddElementToCollection(_testStringArrayList, value);
            Console.WriteLine("Время затраченное на добавление в ArrayList {0} елементов типа System.String равно {1} мс", CountElement, TimeValue.ElapsedMilliseconds);
            TimeValue.Reset();
        }

        public void GetElementStringWithArrayList()
        {
            GetElementOfCollection<String>(_testStringArrayList);
            Console.WriteLine("Время затраченное на получение в ArrayList {0} елементов тип System.String равно {1} мс", CountElement, TimeValue.ElapsedMilliseconds);
            TimeValue.Reset();
        }

        public void SortStringArrayList()
        {
            SortArrayListCollection(_testStringArrayList);
            Console.WriteLine("Время затраченное на сортировку ArrayList {0} елементами тип System.String равно {1} мс", CountElement, TimeValue.ElapsedMilliseconds);
            TimeValue.Reset();
        }

        #endregion

        #region Methods for Int32 in List 

        public void AddElementIntToList()
        {
            AddElementToCollection(_testIntList, 1);
            Console.WriteLine("Время затраченное на добавление в List {0} елементов  типа Int32 равно {1} мс", CountElement, TimeValue.ElapsedMilliseconds);
            TimeValue.Reset();
        }

        public void GetElementIntWithList()
        {
            GetElementOfCollection<Int32>(_testIntList);
            Console.WriteLine("Время затраченное на получение в List {0} елементов типа Int32 равно {1} мс", CountElement, TimeValue.ElapsedMilliseconds);
            TimeValue.Reset();
        }

        public void SortIntList()
        {
            SortListCollection(_testIntList);
            Console.WriteLine("Время затраченное на сортировку List с {0} елементами типа Int32 равно {1} мс", CountElement, TimeValue.ElapsedMilliseconds);
            TimeValue.Reset();
        }

        #endregion

        #region Methods for String in List

        public void AddElementStringToList(string value = "Abc")
        {
            AddElementToCollection(_testStringList, value);
            Console.WriteLine("Время затраченное на добавление в List {0} елементов типа System.String равно {1} мс", CountElement, TimeValue.ElapsedMilliseconds);
            TimeValue.Reset();
        }
        public void GetElementStringWithList()
        {
            GetElementOfCollection<String>(_testStringList);
            Console.WriteLine("Время затраченное на получение в List {0} елементов тип System.String равно {1} мс", CountElement, TimeValue.ElapsedMilliseconds);
            TimeValue.Reset();
        }
        public void SortStringList()
        {
            SortListCollection(_testStringList);
            Console.WriteLine("Время затраченное на сортировку List {0} елементами тип System.String равно {1} мс", CountElement, TimeValue.ElapsedMilliseconds);
            TimeValue.Reset();
        }

        #endregion

    }
}
