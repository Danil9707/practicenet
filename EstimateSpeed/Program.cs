﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstimateSpeed
{
    class Program
    {
        static void Main(string[] args)
        {


            var collectoin = new QualificationCollection();

            Console.WriteLine("Оценка скорости добавления элементов ");

            collectoin.AddElementIntToList();
            collectoin.AddElementStringToList();

            collectoin.AddElementIntToArrayList();
            collectoin.AddElementStringToArrayList();

            Console.WriteLine(new String('_',80));


            Console.WriteLine("Оценка скорости получения элементов ");

            collectoin.GetElementIntWithList();
            collectoin.GetElementStringWithList();

            collectoin.GetElementIntWithArrayList();
            collectoin.GetElementStringWithArrayList();

            Console.WriteLine(new String('_', 80));


            Console.WriteLine("Оценка скорости сортировки элементов ");

            collectoin.SortIntList();
            collectoin.SortStringList();

            collectoin.SortIntArrayList();
            collectoin.SortStringArrayList();

            Console.WriteLine(new String('_', 80));

            Console.ReadLine();
        }
    }
}
