﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApplicationLifeCycle.DTO
{
    public class AdvanceInfo:BaseInfo
    {
        public String Page { get; set; }
        public String IpAdress { get; set; }
    }
}