﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FirstPage.aspx.cs" Inherits="ApplicationLifeCycle.FirstPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <ol>
                <li>
                    <a href="Default.aspx">Главная страница
                    </a>
                </li>
                <li>
                    <a href="SecondPage.aspx">Вторая страница
                    </a>
                </li>
            </ol>
        </div>
    </form>
</body>
</html>
