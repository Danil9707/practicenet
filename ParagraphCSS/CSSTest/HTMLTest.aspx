﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HTMLTest.aspx.cs" Inherits="CSSTest.HTMLTest" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Черно-белый текст</title>
    <link rel="stylesheet" href="Style.css" type="text/css" />
    
</head>
<body>
    <form id="form1" runat="server">
        <div class="OuterBorder">
            <p class="leftBlack">BLACK</p>
            <p class="rightWhite"> WHITE</p>
        </div>
    </form>
</body>
</html>
